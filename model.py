from tensorflow.keras.models import Model
from tensorflow.keras.datasets import mnist
from tensorflow.keras.layers import Input,Add, Conv2DTranspose, concatenate, Activation, MaxPooling2D, Conv2D, BatchNormalization
from tensorflow.keras import backend as K
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import utils


from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications import VGG16, EfficientNetB0, ResNet101, DenseNet121
import numpy as np
from PIL import Image
import tensorflow as tf
from  tensorflow.keras.losses import Loss

def dice_coeff(y_true, y_pred):
    smooth = 100
    # Flatten
    y_true_f = tf.cast(tf.reshape(y_true, [-1]),'float32')
    y_pred_f = tf.cast(tf.reshape(y_pred > 0.5, [-1]),'float32')

    intersection = tf.reduce_sum(tf.math.multiply(y_true_f,y_pred_f))
    score = (2. * intersection + smooth) / (tf.reduce_sum(y_true_f) + tf.reduce_sum(y_pred_f) + smooth)
    return score
def multi_class_dice_loss(y_true, y_pred,logits_size):

    def _compute_dice_loss(flat_input, flat_target):
        alpha=0.2
        smooth=1e-8
        flat_input = ((1.0 - flat_input) ** alpha) * flat_input
        interection = K.sum(flat_input * flat_target, -1)
        loss = 1.0 - ((2.0 * interection + smooth) /(K.sum(flat_input) + K.sum(flat_target) + smooth))
        return loss

    loss=0.0
    logits_size = tf.shape(y_pred)[1]
    for label_idx in range(logits_size):
        flat_input_idx = y_pred[:, label_idx]
        flat_target_idx = y_true[:, label_idx]
        loss_idx = _compute_dice_loss(flat_input_idx, flat_target_idx)

        loss += loss_idx

    return loss


class Dice(Loss):
    def __init__(self, logits_size=2):
        super(Dice).__init__()
        self.logits_size = logits_size
        self._name_scope = 'dice'
        self.reduction = 'auto'

    def multi_class_dice_loss(self, y_true, y_pred):
        def _compute_dice_loss(flat_input, flat_target):
            alpha = 0.2
            smooth = 1e-8
            flat_input = ((1.0 - flat_input) ** alpha) * flat_input
            interection = K.sum(flat_input * flat_target, -1)
            loss = 1.0 - ((2.0 * interection + smooth) / (K.sum(flat_input) + K.sum(flat_target) + smooth))
            return loss

        loss = 0.0
        # print('pred shape:',tf.shape(y_pred),tf.shape(y_true))
        for label_idx in range(self.logits_size):
            flat_input_idx = y_pred[:, label_idx]
            flat_target_idx = y_true[:, label_idx]
            loss_idx = _compute_dice_loss(flat_input_idx, flat_target_idx)
            
            loss += loss_idx
            print(loss)

        return loss

    def call(self, y_true, y_pred):
        return self.multi_class_dice_loss(y_true, y_pred)



def bloc_transpose(x,block_out, numb_filter = [256,128], BN = 1):
  #x, - input tensor
  #block_out, - scip tensor
  #numb_filter = [256,128] - number of kernel in layers
  x = concatenate([x, block_out])
  x = Conv2D(numb_filter[0], (3, 3), padding='same')(x)
  if BN:
      x = BatchNormalization()(x)
  x = Activation('relu')(x)

  x = Conv2D(numb_filter[0], (3, 3), padding='same')(x)
  if BN:
      x = BatchNormalization()(x)
  x = Activation('relu')(x)

  # UP
  x = Conv2DTranspose(numb_filter[1], (2, 2), strides=(2, 2), padding='same')(x)
  if BN:
      x = BatchNormalization()(x)
  x = Activation('relu')(x)
  return x

def bloc_transpose_li(x,block_out, numb_filter = [256,128], BN = 1):
  #x, - input tensor
  #block_out, - scip tensor
  #numb_filter = [256,128] - number of kernel in layers
  block_out = Conv2D(numb_filter[0], (3, 3), padding='same')(block_out)
  x = Conv2D(numb_filter[0], (3, 3), padding='same')(x)

  x = Add()([x, block_out])

  if BN:
      x = BatchNormalization()(x)
  x = Activation('relu')(x)

  x = Conv2D(numb_filter[0], (3, 3), padding='same')(x)
  if BN:
      x = BatchNormalization()(x)
  x = Activation('relu')(x)

  # UP
  x = Conv2DTranspose(numb_filter[1], (2, 2), strides=(2, 2), padding='same')(x)
  if BN:
      x = BatchNormalization()(x)
  x = Activation('relu')(x)
  return x

def bloc_downsampling(x, numb_filter = [256,128], BN = 1, pooling = 1):
  #x, - input tensor
  #block_out, - scip tensor
  #numb_filter = [256,128] - number of kernel in layers
  x = Conv2D(numb_filter[0], (3, 3), padding='same')(x)
  if BN:
      x = BatchNormalization()(x)
  x = Activation('relu')(x)

  x = Conv2D(numb_filter[0], (3, 3), padding='same')(x)
  if BN:
      x = BatchNormalization()(x)
  x = Activation('relu')(x)

  # UP
  if pooling == 1:
      x = Conv2D(numb_filter[1], (2, 2), strides=(2, 2), padding='same')(x)
  elif pooling == 2:
      x = MaxPooling2D(padding='same')(x)

  if BN:
      x = BatchNormalization()(x)
  x = Activation('relu')(x)
  return x




def unet_pre_train(num_classes=13, input_shape=(200, 600, 3), pre_trained_model=None, pretrain_out=13,
                   list_bloc=[10, 6, 3], BN=1, nf = 1):
    block_out = []
    if pre_trained_model != None:
        for layer in pre_trained_model.layers[:len(pre_trained_model.layers)]:
            layer.trainable = False
        x = pre_trained_model.layers[pretrain_out].output  # конец сверток
        block_out = [pre_trained_model.layers[i].output for i in list_bloc]

        img_input = pre_trained_model.inputs
        print(block_out[0])
        # первое уменьшение размера
    else:
        x = Input(shape=input_shape)
        img_input = x
        n = len(list_bloc)-1
        #x = bloc_downsampling(x, numb_filter=[256 // (2 ** (n - i)), 128 // (2 ** (n - i))], BN=BN, pooling=0)
        for i,_ in enumerate(list_bloc):
            x = bloc_downsampling(x, numb_filter=[256 // nf // (2 ** (n-i)), 128 //nf // (2 ** (n-i))], BN = BN)
            block_out.insert(0, x)
        # UP 1

    # добавили перенос из понижаюшего плеча VGG16
    for i, block_i in enumerate(block_out):
        x = bloc_transpose(x, block_i, numb_filter=[256 // nf // 2 ** i, 128 // nf // 2 ** i], BN = BN)

    x = Conv2D(64, (3, 3), padding='same')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    if num_classes>1:
        x1 = Conv2D(1, (3, 3), activation='relu', padding='same')(x)
        x2 = Conv2D(num_classes, (3, 3), activation='softmax', padding='same')(x)
    else:
        x1 = Conv2D(1, (3, 3),  padding='same',activation='relu')(x)#activation='relu',,
        x2 = Conv2D(1, (3, 3), padding='same',activation='sigmoid')(x)  # ,

    model = Model(img_input, [x1,x2])
    model.compile(optimizer=Adam( learning_rate = 0.001),
                  loss='mse',
                  metrics=['mse'])
    model.summary()
    return model

def unet_pre_train_seg(num_classes=13, input_shape=(200, 600, 3), pre_trained_model=None, pretrain_out=13,
                   list_bloc=[10, 6, 3], BN=1, nf = 1):
    block_out = []
    if pre_trained_model != None:
        for layer in pre_trained_model.layers[:len(pre_trained_model.layers)]:
            layer.trainable = False
        x = pre_trained_model.layers[pretrain_out].output  # конец сверток
        block_out = [pre_trained_model.layers[i].output for i in list_bloc]

        img_input = pre_trained_model.inputs
        print(block_out[0])
        # первое уменьшение размера
    else:
        x = Input(shape=input_shape)
        img_input = x
        n = len(list_bloc)-1
        #x = bloc_downsampling(x, numb_filter=[256 // (2 ** (n - i)), 128 // (2 ** (n - i))], BN=BN, pooling=0)
        for i,_ in enumerate(list_bloc):
            x = bloc_downsampling(x, numb_filter=[256 // nf // (2 ** (n-i)), 128 //nf // (2 ** (n-i))], BN = BN)
            block_out.insert(0, x)
        # UP 1

    # добавили перенос из понижаюшего плеча VGG16
    for i, block_i in enumerate(block_out):
        x = bloc_transpose(x, block_i, numb_filter=[256 // nf // 2 ** i, 128 // nf // 2 ** i], BN = BN)

    x = Conv2D(64, (3, 3), padding='same')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    if num_classes>1:
        #x1 = Conv2D(1, (3, 3), activation='relu', padding='same')(x)
        x2 = Conv2D(num_classes, (3, 3), activation='softmax', padding='same')(x)
    else:
        #x1 = Conv2D(1, (3, 3),  padding='same',activation='relu')(x)#activation='relu',,
        x2 = Conv2D(1, (3, 3), padding='same',activation='sigmoid')(x)  # ,

    model = Model(img_input, x2)
    model.compile(optimizer=Adam( learning_rate = 0.001),
                  loss=tf.keras.losses.CategoricalCrossentropy(),
                  metrics=['mse'])
    model.summary()
    return model

#main net (target only)
def LiNet_pre_train_seg(num_classes=13, input_shape=(200, 600, 3), pre_trained_model=None, pretrain_out=13,
                   list_bloc=[10, 6, 3], BN=1, nf = 1, train_reg= 1):
    block_out = []
    if pre_trained_model != None:
        for layer in pre_trained_model.layers[:len(pre_trained_model.layers)]:
            layer.trainable = False
        x = pre_trained_model.layers[pretrain_out].output  # конец сверток
        block_out = [pre_trained_model.layers[i].output for i in list_bloc]

        img_input = pre_trained_model.inputs
        print(block_out[0])
        # первое уменьшение размера
    else:
        x = Input(shape=input_shape)
        img_input = x
        n = len(list_bloc)-1
        #x = bloc_downsampling(x, numb_filter=[256 // (2 ** (n - i)), 128 // (2 ** (n - i))], BN=BN, pooling=0)
        for i,_ in enumerate(list_bloc):
            x = bloc_downsampling(x, numb_filter=[256 // nf // (2 ** (n-i)), 128 //nf // (2 ** (n-i))], BN = BN)
            block_out.insert(0, x)
        # UP 1

    # добавили перенос из понижаюшего плеча VGG16
    for i, block_i in enumerate(block_out):
        x = bloc_transpose_li(x, block_i, numb_filter=[256 // nf // 2 ** i, 128 // nf // 2 ** i], BN = BN)
    x1 = x
    x = Conv2D(64, (3, 3), padding='same')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    if num_classes>1:
        #x1 = Conv2D(1, (3, 3), activation='relu', padding='same')(x)
        x2 = Conv2D(num_classes, (3, 3), activation='softmax', padding='same')(x)
    else:
        #x1 = Conv2D(1, (3, 3),  padding='same',activation='relu')(x)#activation='relu',,
        x2 = Conv2D(1, (3, 3), padding='same',activation='sigmoid')(x)  # ,

    model = Model(img_input, x2)

    model.compile(optimizer=Adam( learning_rate = 0.001),
                  loss=tf.keras.losses.CategoricalCrossentropy(),
                  metrics=['mse'])
    model.summary()
    if train_reg:
        return model
    else:
        model_base = Model(img_input, x1)
        return model, model_base

def LiNet_pre_train_seg_pretrain(model_name, num_classes=2,num_class_seg=20, num_classes_depth = 1, input_shape=(512, 1024, 3), BN=1, nf = 1, frozen = True):
    modelu, modele_base = LiNet_pre_train_seg(num_classes=2, input_shape=(512, 1024, 3), pre_trained_model=None, pretrain_out=1,
                   list_bloc=[10]*list_n, BN=BN, nf=nf, train_reg= 0)
    #modelu.summary()
    try:
        modelu.load_weights(model_name)
    except:
        pass
    x = Input(shape=input_shape)
    img_input = x
    x = modele_base(x)
    if frozen:
        for layer in modelu.layers:
            layer.trainable = False

    x3 = Conv2D(64, (3, 3), padding='same')(x)
    x3 = BatchNormalization()(x3)
    x3 = Activation('relu')(x3)
    if num_classes_depth>1:
        x3 = Conv2D(1, (3, 3), padding='same',activation='relu',name = 'depth')(x3)

    x2 = Conv2D(64, (3, 3), padding='same')(x)
    x2 = BatchNormalization()(x2)
    x2 = Activation('relu')(x2)

    if num_classes>1:
        #x1 = Conv2D(1, (3, 3), activation='relu', padding='same')(x)
        x2 = Conv2D(num_classes, (3, 3), activation='softmax', padding='same',name = 'segment')(x2)
    else:
        #x1 = Conv2D(1, (3, 3),  padding='same',activation='relu')(x)#activation='relu',,
        x2 = Conv2D(1, (3, 3), padding='same',activation='sigmoid',name = 'segment')(x2)  # ,

    if frozen:
        x1 = modelu(x)
    else:
        x1 = Conv2D(64, (3, 3), padding='same')(x)
        x1 = BatchNormalization()(x1)
        x1 = Activation('relu')(x1)
        if num_class_seg>1:
            #x1 = Conv2D(1, (3, 3), activation='relu', padding='same')(x)
            x1 = Conv2D(num_class_seg, (3, 3), activation='softmax', padding='same',name = 'target')(x1)
        else:
            #x1 = Conv2D(1, (3, 3),  padding='same',activation='relu')(x)#activation='relu',,
            x1 = Conv2D(1, (3, 3), padding='same',activation='sigmoid',name = 'target')(x1)


    model = Model(img_input, [x2,x1,x3])
    model.compile(optimizer=Adam( learning_rate = 0.001),
                  loss={'depth': tf.keras.losses.MeanSquaredError(),'segment':tf.keras.losses.MeanSquaredError(),'target':tf.keras.losses.MeanSquaredError()},
                  metrics=['mse'])

    model.summary()
    return model

# model target+segment
def LiNet_pre_train_seg_2(num_classes=13, input_shape=(128, 256, 3), pre_trained_model=None,
                          modelt_weight='new_128_256.hdf5', pretrain_out=1,
                          list_bloc=[10, 6, 3], BN=1, nf=2, train_reg=1):
    block_out = []
    if pre_trained_model != None:
        for layer in pre_trained_model.layers[:len(pre_trained_model.layers)]:
            layer.trainable = False
        x = pre_trained_model.layers[pretrain_out].output  # конец сверток
        block_out = [pre_trained_model.layers[i].output for i in list_bloc]

        img_input = pre_trained_model.inputs
        print(block_out[0])
        # первое уменьшение размера
    else:
        x = Input(shape=input_shape)
        img_input = x
        n = len(list_bloc) - 1
        # x = bloc_downsampling(x, numb_filter=[256 // (2 ** (n - i)), 128 // (2 ** (n - i))], BN=BN, pooling=0)
        for i, _ in enumerate(list_bloc):
            x = bloc_downsampling(x, numb_filter=[256 // nf // (2 ** (n - i)), 128 // nf // (2 ** (n - i))],
                                        BN=BN)
            block_out.insert(0, x)
        # UP 1

    # добавили перенос из понижаюшего плеча VGG16
    for i, block_i in enumerate(block_out):
        x = bloc_transpose_li(x, block_i, numb_filter=[256 // nf // 2 ** i, 128 // nf // 2 ** i], BN=BN)
    x1 = x
    x = Conv2D(64, (3, 3), padding='same')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    x1 = Conv2D(64, (3, 3), padding='same')(x1)
    x1 = BatchNormalization()(x1)
    x1 = Activation('relu')(x1)

    if num_classes > 1:
        # x1 = Conv2D(1, (3, 3), activation='relu', padding='same')(x)
        x2 = Conv2D(2, (3, 3), activation='softmax', padding='same', name="target_output")(x)
        x3 = Conv2D(num_classes, (3, 3), activation='softmax', padding='same', name="class_output")(x1)
    else:
        # x1 = Conv2D(1, (3, 3),  padding='same',activation='relu')(x)#activation='relu',,
        x2 = Conv2D(1, (3, 3), padding='same', activation='sigmoid', name="target_output")(x)  # ,
        x3 = Conv2D(num_classes, (3, 3), activation='softmax', padding='same', name="class_output")(x1)

    modelt = Model(img_input, x2)

    modela = Model(img_input, [x2, x3])
    modelt.load_weights(modelt_weight)
    modelt.trainable = False
    losses = {"target_output": Dice(2),  # "categorical_crossentropy",
              "class_output": Dice(14),
              # tf.keras.losses.CategoricalCrossentropy(),#tf.keras.losses.SparseCategoricalCrossentropy(),
              }
    lossWeights = {"target_output": 0.0, "class_output": 1.0}
    # initialize the optimizer and compile the model
    print("[INFO] compiling model...")

    modela.compile(optimizer=Adam(learning_rate=0.01, decay=0.9), loss=losses, loss_weights=lossWeights,
                   metrics=["accuracy", dice_coeff])

    #     model.compile(optimizer=Adam( learning_rate = 0.001),
    #                   loss={tf.keras.losses.CategoricalCrossentropy()},
    #                   metrics=['mse'])
    modela.summary()
    if train_reg:
        return modela, modelt
    else:
        model_base = Model(img_input, x1)
        return modela, model_base

# model for target, segment, depth
def LiNet_pre_train_seg_3(num_classes=13, input_shape=(128, 256, 3), pre_trained_model=None,
                          modelt_weight='model/new_128_256_25_04.hdf5', pretrain_out=1,
                          list_bloc=[10, 6, 3], BN=1, nf=2, train_reg=1):
    block_out = []
    if pre_trained_model != None:
        for layer in pre_trained_model.layers[:len(pre_trained_model.layers)]:
            layer.trainable = False
        x = pre_trained_model.layers[pretrain_out].output  # конец сверток
        block_out = [pre_trained_model.layers[i].output for i in list_bloc]

        img_input = pre_trained_model.inputs
        print(block_out[0])
        # первое уменьшение размера
    else:
        x = Input(shape=input_shape)
        img_input = x
        n = len(list_bloc) - 1
        # x = bloc_downsampling(x, numb_filter=[256 // (2 ** (n - i)), 128 // (2 ** (n - i))], BN=BN, pooling=0)
        for i, _ in enumerate(list_bloc):
            x = bloc_downsampling(x, numb_filter=[256 // nf // (2 ** (n - i)), 128 // nf // (2 ** (n - i))],
                                        BN=BN)
            block_out.insert(0, x)
        # UP 1

    # добавили перенос из понижаюшего плеча VGG16
    for i, block_i in enumerate(block_out):
        x = bloc_transpose_li(x, block_i, numb_filter=[256 // nf // 2 ** i, 128 // nf // 2 ** i], BN=BN)
    x1 = x
    x2 = x
    x0 = x
    x_latent =  x

    #segment
    x1 = Conv2D(64, (3, 3), padding='same')(x1)
    x1 = BatchNormalization()(x1)
    x1 = Activation('relu')(x1)
    # target depth
    x2 = Conv2D(64, (3, 3), padding='same')(x1)
    x2 = BatchNormalization()(x1)
    x2 = Activation('relu')(x1)
    # target
    x0 = Conv2D(64, (3, 3), padding='same')(x0)
    x0 = BatchNormalization()(x0)
    x0 = Activation('relu')(x0)

    if num_classes > 1:
        # x1 = Conv2D(1, (3, 3), activation='relu', padding='same')(x)
        xt = Conv2D(2, (3, 3), activation='softmax', padding='same', name="target_output")(x0)
        xs = Conv2D(num_classes, (3, 3), activation='softmax', padding='same', name="class_output")(x1)
        xd = Conv2D(1, (3, 3), activation='relu', padding='same', name="depth_output")(x2)
    else:
        # x1 = Conv2D(1, (3, 3),  padding='same',activation='relu')(x)#activation='relu',,
        xt = Conv2D(1, (3, 3), padding='same', activation='sigmoid', name="target_output")(x0)  # ,
        xs = Conv2D(num_classes, (3, 3), activation='softmax', padding='same', name="class_output")(x1)
        xd = Conv2D(1, (3, 3), activation='relu', padding='same', name="depth_output")(x2)

    modelt = Model(img_input, xt)
    modelts = Model(img_input, [xt, xs])
    modela = Model(img_input, [xt, xs, xd])
    modelt.load_weights(modelt_weight)
    modelt.trainable = False
    losses = {"target_output": Dice(2),  # "categorical_crossentropy",
              "class_output": Dice(14),
              "depth_output": 'mse',
              # tf.keras.losses.CategoricalCrossentropy(),#tf.keras.losses.SparseCategoricalCrossentropy(),
              }
    lossWeights = {"target_output": 0.0, "class_output": 1.0, "depth_output":1.0}
    # initialize the optimizer and compile the model
    print("[INFO] compiling model...")

    modela.compile(optimizer=Adam(learning_rate=0.01, decay=0.9), loss=losses, loss_weights=lossWeights,
                   metrics=["accuracy", dice_coeff])

    #     model.compile(optimizer=Adam( learning_rate = 0.001),
    #                   loss={tf.keras.losses.CategoricalCrossentropy()},
    #                   metrics=['mse'])
    modela.summary()
    if train_reg:
        return modela, modelt, modelts
    else:
        model_base = Model(img_input, x_latent)
        return modela, model_base
def convert(modelu,save_dir):

    tf.saved_model.save(modelu, save_dir)

    # Convert the model
    converter = tf.lite.TFLiteConverter.from_saved_model(save_dir)  # path to the SavedModel directory
    tflite_model = converter.convert()

    converter_float32 = tf.lite.TFLiteConverter.from_keras_model(modelu)

    converter_float32.optimizations = [tf.lite.Optimize.DEFAULT]
    tflite_model_quant_float32 = converter_float32.convert()
    tflite_model_quant_file_float32 =  'mod_FLOAT32' + '.tflite'
    print(tflite_model_quant_file_float32)
    tflite_model_quant_path_float32 = os.path.join(save_dir, tflite_model_quant_file_float32)
    print(tflite_model_quant_path_float32)
    with open(tflite_model_quant_path_float32, "wb") as f:
        f.write(tflite_model_quant_float32)


    # Save the model.
    with open(save_dir + '/' + 'model.tflite', 'wb') as f:
        f.write(tflite_model)
    print('Conversion Successful. File written to ', tflite_model_quant_path_float32)

def inference(tflite_file_path ='model/model.tflite'):
    """

    :type tflite_file_path: string
    """
    # Load the TFLite model in TFLite Interpreter
    interpreter = tf.lite.Interpreter(tflite_file_path)
    #interpreter.allocate_tensors()
    # There is only 1 signature defined in the model,
    # so it will return it by default.
    # If there are multiple signatures then we can pass the name.
    my_signature = interpreter#.get_signature_runner()
    return my_signature



if __name__ == '__main__':
    pre_trained_model = VGG16(input_shape=(200, 600, 3), include_top=False, weights="imagenet")
