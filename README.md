# motomul 2.0


## Getting started
 - ардуино код https://disk.yandex.ru/d/OhjTr56mj5x1rQ
 - образ системы для джетсона https://developer.nvidia.com/embedded/l4t/r32_release_v7.1/jp_4.6.1_b110_sd_card/jeston_nano/jetson-nano-jp461-sd-card-image.zip
 - установить зависимости
 - подключить камеру, MPU6050, Arduino
 - запустить python3 main.py (python3 main_pipeline_cam.py)
 - 83-webcam.rules to /lib/udev/rules.d

Состав проекта:
    
- main.py - основной запускающий 3 рабочих процесса модуль
- рабочие процессы:
        
    - main_pipeline_cam.py - обработка информации от камеры
    - main_pipeline_bins.py - получение данных с БИНС (MPU6050)
    - main_pipeline.py -  основной контур управления (получает данные от main_pipeline_cam.py, main_pipeline_bins.py через область памяти )

- модули:
    - net_target3.py - обработка кадра полученного из камеры
    - control.py - обработка данных и выработка управляющего уровня по скорости и углу
    - config.py - конфигурация модуля
    - model.py - схемы нейросетей
    - framequality.py - оценка качества кадра

    - 10_08_2023_UZD.ino - управление датчиками УЗД ()

    - BBBB16.ino - управление двигателем ()

- Форма маяков (мишени) : https://gitlab.com/mkorlyakova69/motomul-2.0/-/blob/main/data/%D0%9C%D0%B0%D1%8F%D0%BA3_2.jpg, https://gitlab.com/mkorlyakova69/motomul-2.0/-/blob/main/data/mayak6.pdf

- СТРУКТУРА СООБЩЕНИЙ КОНТРОЛЛЕР-JETSON : https://gitlab.com/mkorlyakova69/motomul-2.0/-/blob/main/data/%D0%A2%D0%B0%D0%B1%D0%BB%D0%B8%D1%86%D0%B0_%D0%B2%D0%BD%D0%B5%D1%88%D0%BD%D0%B8%D1%85_%D1%81%D0%B2%D1%8F%D0%B7%D0%B5%D0%B9.xlsx

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/mkorlyakova69/motomul-2.0.git
git branch -M main
git push -uf origin main
```

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

В проекте нет кода и данных для обучения нейронной сети

В /model лежат веса нейронной сети соответсвующего установленной модели размера, но эта сеть не полностью обучена(т.е. результат ее работы не соответсвует нашему рабочему образце)



## Name
Motomul 2.0

## Description
ПО для управления автономной подвижной платформой Мотомул


## Roadmap
ПЛАН РАЗРАБОТКИ:

Состав аппаратно-программной части системы:
	Jetson Nano + WEB camera + MPU 6050
	Контроллер1 + УЗД
	Контроллер2 управление двигателем и общей логикой взаимодействия с пультом управления 
	2 двигателя движения
	1 двигатель актуатора

	пульт управления (кнопочный)
	пульт управления дистанционный (радиоуправление)

	механическая часть системы
	электрическая часть системы (аккумуляторы+окружение для подключения всех узлов)
	
	программная часть:
	Ubuntu 20.04
	Python + C++(Контроллер)
	TensorFlow 2.9
	OpenCV 4.x

	Работают параллельно (ассинхронно)
	Jetson:
    • Модуль1 : модуль получения информации от камеры
    • Модуль2 : модуль получения информации от БИНС
    • Модуль3 : модуль обработки информации от камеры и БИНС и генерации сообщений для  Контроллер2
	Модуль4 : модуль обработки информации в Контроллер2 (логика управления низкого уровня)
	Модуль5 : модуль обработки информации в Контроллер1 (опрос датчиков УЗД и генерация сигнала «СТОП» при экстренном торможении — требует доработки)
	
	ВЗАИМОДЕЙСТВИЯ:
		Jetson(Модуль3) — Модуль4
		Модуль5 - Модуль4	
		Jetson(Модуль3) с Jetson(Модуль1)
		Jetson(Модуль3) с Jetson(Модуль2)

Что есть:

    1. Интеллектуальный датчик возвращает информацию с частотой 10гЦ (в процессе отладки):
    • 	Дальность и угол в горизонтали кадра до маркера
    • 	дальность до объектов сцены по всему полю видимой камерой области : (размер кадра 128х256)
    • состав сцены (сегменты) в объеме разметки CityScape (20 классов: машина, человек, дома, деревья …) (размер кадра 128х256)

    2. Процедура калибровки камеры
    3. Процедура оценки качества кадра (доработать до изменения параметров текущего диапазона качества от походной калибровки по состоянию сцены)
    4. Рабочий процесс управления от Контроллер2(в процессе отладки — нужна обратная связь от камеры)
    5. Рабочий процесс управления от Контроллер1(в процессе отладки -  передавать данные УЗД)

    6. Вычисление траектории метки в поле зрения камеры в 3D траекторию относительно модуля оценки траектории:
    • прямое управление — ехать на метку (работает почти)
    • накапливается и сохраняется траектория по результатам обработки датчиков (в процессе отработки)
    • проблема измерения текущей собственной траектории (п.7. в отладка)

    7. Вычисление собственных параметров движения в абсолютной системе координат:
    • Накапливается и сохраняется траектория по результатам обработки датчиков (в процессе отладки)
    8. Генерация сообщений для Arduino2 для управления движением (есть вроде бы рабочая логика, есть проблемы организации этой логики, это работает с ограничениями и проблемами):
    • проблемы с задержками передач
    • проблемы с порядком накопления буфера сообщений в Arduino2
    • проблемы с логикой действий Arduino2 
    9. Запись кадров с особыми объектами по маршруту и вычисление «вектора» этих кадров (состав сцены+глубина):
    • Отбор кадров (в процессе разработки)
    • Сравнение кадров между собой( в процессе разработки)
    • Хранение (ПРЯМОЕ УКАЗАНИЕ ИМЕН)
    • Хранение на диске (нужно кеширование? И подтягивание актуальных данных маршрута)
    10. Возвращение по сохраненному маршруту(ЭТО ТОЛЬКО ПРОЕКТИРУЕМАЯ ПРОЦЕДУРА)

Цель : работа единым целым
ЧТО НЕОБХОДИМО ДЕЛАТЬ:

    • обмен данными в Jetson(уйти от SharedMemory)
    • управление от телефона
    • хранение данных и доступ к ним (п.9 и 10.)
    • разворачивание


Форма маяков (мишени) : https://gitlab.com/mkorlyakova69/motomul-2.0/-/blob/main/data/%D0%9C%D0%B0%D1%8F%D0%BA3_2.jpg, https://gitlab.com/mkorlyakova69/motomul-2.0/-/blob/main/data/mayak6.pdf
	      
## Contributing


## Authors and acknowledgment
Mariya Korlyakova, Ekaterina Korlyakova, Kirill Klimenko, Mariya Ivanova, Daniil Litovchenko

## License


## Project status
