import numpy as np
import traek_test_progress
#import net_target
import time


def kate_system_control_old(detector_frame,  xwa, v = 0 ):
    angle = detector_frame
    if detector_frame[1]:
        angle=detector_frame[0]
        distance=detector_frame[1]
        v=detector_frame[2]
        #anglex_c, r_c = traek_test_progress.traectory_move(angle, distance, v)
        ### for no thinking 28/05 start
        anglex_c=detector_frame[0]
        r_c=detector_frame[1]
        if (r_c>10):
            if (r_c>100):
                r_c=2
            else:
                r_c=1
        else:
            r_c=0
        v=detector_frame[2]
        ### for no thinking 28/05 finish
        return detector_frame[1], anglex_c, r_c, v
    else:
        return False, 0,0
        
def kate_system_control(detector_frame,  xwa, v = 0, logger = None ):
    """
    Control from cam-bins information
    :param detector_frame:  cam result
    :param xwa: bins result
    :param v: current velocity
    :param logger: loger object
    :return: [detect, velocity (0,1,2), angle(-20, 20)]
    """

    angle,distance,flag = detector_frame
    if  logger is not None:
        logger.info('kate1: '+str(angle)+'_'+str(distance)+str(flag))
    if flag[0]:
        #if  logger is not None:
        #    logger.info('kate2: '+str(angle)+'_'+str(distance))
        anglex_c, r_c = traek_test_progress.traectory_move(angle, distance, v, logger = logger)
        logger.info('katecontrol: return:' + str(anglex_c) + ', ' + str(r_c) + ' : input : ' + str( angle)+'_'+str(distance)+'_'+str(v))
        anglex_c, r_c = traek_test_progress.traectory_simple(angle, distance, v )
        logger.info('katecontrol simple: return:' + str(anglex_c) + ', ' + str(r_c) + ' : input : ' + str(angle) + '_' + str(distance) + '_' + str(v))
        if  logger is not None:
            logger.info('kate3: '+str(anglex_c)+'_'+str(r_c))
        return detector_frame[2], anglex_c, r_c
    else:
        return [False, False], 0,0

