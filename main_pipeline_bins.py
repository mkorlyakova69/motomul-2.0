import sys
#from pynput import keyboard
from multiprocessing import shared_memory
import time
import numpy as np

import config

import logging

import time


logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')
file_handler = logging.FileHandler('logs_bins.log')
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


if config.JETSON:
    import board
    import adafruit_mpu6050
    try:
        #https: // automaticaddison.com / visualize - imu - data - using - the - mpu6050 - ros - and -jetson - nano /
        i2c = board.I2C()  # uses board.SCL and board.SDA
        # i2c = board.STEMMA_I2C()  # For using the built-in STEMMA QT connector on a microcontroller
        mpu = adafruit_mpu6050.MPU6050(i2c)
        logger.info('log mpu6050')
    except:
        logger.error('error mpu6050 open.')
else:
    mpu = {"acceleration":[0.,0.,0.],"gyro":[0.,0.,0.],'temperature':25.}


def get_mpu(mpu):
        try:
            logger.info("Acceleration: X:%.2f, Y: %.2f, Z: %.2f m/s^2" % (mpu.acceleration))
            logger.info("Gyro X:%.2f, Y: %.2f, Z: %.2f rad/s" % (mpu.gyro))
            logger.info("Temperature: %.2f C" % mpu.temperature)
            logger.info("")
            a = mpu.acceleration
            g = mpu.gyro
            t = mpu.temperature
            #print(a,g,t)
        except:
            a = 0
            g = 0
            t = 0
        time.sleep(0.1)
        return a, g, t
# logger.info('log message')
# logger.error('error message.')

if 1:
    dict_conf = config.confir_read()
    # parse arguments
    temp_dir = dict_conf["temp_dir"]  #
    time_calib_cam = dict_conf["time_calib_cam"]
    time_calib_gyro = dict_conf["time_calib_gyro"]
    time_interval_detect = dict_conf["time_interval_detect"]#
    image_size_detect = dict_conf["image_size_detect"]  #
    name_detect_net = dict_conf["name_detect_net"]

    time_interval_gyro =dict_conf["time_interval_gyro"]

    N=dict_conf["N_BINS"]
    # time_bins, x, y, z, vx, vy, vz, ax, ay, az, lx, ly, lz,REZERV, calibr,
    BINS = np.array([[time.time(), 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,0.,0.,0.,0.,0.]]*N)
    try:
        shm = shared_memory.SharedMemory(name='pipeline_bins', create=True, size=BINS.nbytes)
    except:
        shm = shared_memory.SharedMemory(name='pipeline_bins', create=False, size=BINS.nbytes)
        #print('BINS.shape:',BINS.shape)
    B_BINS = np.ndarray(BINS.shape, dtype=BINS.dtype, buffer=shm.buf)
    B_BINS[:] = BINS[:]
    logger.info('gyro B_Bins 1')

def gyro_calib(time_interval_gyro):
    calib_gyro = {"ax":0,"ay":0, "az":0,"gx":0,"gy":0, "gz":0}
    return calib_gyro

def write_bins( bins = [0.]*15 ):
        global BINS
        global B_BINS
        logger.info('gyro B_Bins 9:'+str(bins[0]))
        BINS[:-1,:] = BINS[1:,:]
        BINS[-1,:] = bins
        logger.info('gyro B_Bins 5:'+str(bins[0]))
        B_BINS[:] = BINS[:]
        logger.info('gyro B_Bins 2:'+str(bins[0]))
      


def bins_distance(dict_calib_gyro, mpu= mpu):
    anglex, angley, r = 0,0,0
    try:
        a,g,t = get_mpu(mpu)
        logger.info('gyro B_Bins 3:'+str(a[0]))
        flag = 1
    except:
    	flag = 0
    	a = (0,0,0)
    	g =  (0,0,0)
    	logger.error('gyro B_Bins 4:')
    	    
    #TO DO:
    # time_bins, x, y, z, vx, vy, vz, ax, ay, az, lx, ly, lz,REZERV, calibr,
    
    # set x,v,angles from a,g
    return a,g,1 # !!!!!!!!!!!!


def to_control(data):
    """
    bins to control system
    Args:
        detector_frame: detector coord
        data: image data

    Returns:

    """
    #
   
    a, g, flag = data
    # ******* TO DO:
    bins = np.array([[time.time(), a[0], a[1],a[2], g[0], g[1], g[2], 1.0, 1.0, 1.0,0.,0.,0.,0.,flag]])
    logger.info('gyro B_Bins 7:'+str(flag))
    write_bins(bins = bins )
    #print('bins:',bins)
    success = True
    logger.info('gyro B_Bins 8:'+str(success))
    return success

detect = 1



# calibrate
try:
    calib_gyro = gyro_calib(time_calib_gyro )
except:
    print('calibr error')
print('calibr end')
#with keyboard.Events() as events:
# MAIN CYCLE
while True:
#k = 0
#while k<100:

        #k += 1
        try:

            anglex,angley, r = bins_distance(calib_gyro)
            data = (anglex,angley, r)
            logger.info('gyro B_Bins 6:'+str(r))
            if not to_control(data):
                print('error')

        except:
            print('error bins')
            
shm.close()
shm.unlink()            
            
