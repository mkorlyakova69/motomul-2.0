import os.path


#from pynput import keyboard
from multiprocessing import shared_memory
import time
import numpy as np



import cv2

# от main_pipeline_cam.py отличается только версией net_target5
import net_target5 as net_target
import os

import config

from  framequality import FrameQualityEstimator
import logging
import new_control

logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')

file_handler = logging.FileHandler('logs_cam.log')
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


# logger.info('log message')
# logger.error('error message.')

if 1:
    dict_conf = config.confir_read()
    # parse arguments
    temp_dir = dict_conf["temp_dir"]  #
    time_calib_cam = dict_conf["time_calib_cam"]
    time_calib_gyro = dict_conf["time_calib_gyro"]
    time_interval_detect = dict_conf["time_interval_detect"]#
    image_size_detect = dict_conf["image_size_detect"]  #
    name_detect_net = dict_conf["name_detect_net"]

    time_interval_gyro =dict_conf["time_interval_gyro"]

    N=dict_conf["N_CAM"]
    DIR_CAM = dict_conf["DIR_CAM"]
    DIR_AB = dict_conf["DIR_AB"]
    DIR_BA = dict_conf["DIR_BA"]
    DIR_A = dict_conf["DIR_A"]
    DIR_B = dict_conf["DIR_B"]
    DIR_A1 = dict_conf["DIR_A1"]
    DIR_CALIB = dict_conf["DIR_CALIB"]

    ## time, frame Quality, Detector,Angle,Distance,problem, calibr, rezerv]
    CAM = np.array([[time.time(), 0,0, 0,  0, 1, 0, 0]] * N)
    print(CAM.shape)
    try:
        shm = shared_memory.SharedMemory(name='pipeline_cam', create=True, size=CAM.nbytes)
    except:
        shm = shared_memory.SharedMemory(name='pipeline_cam', create=False, size=CAM.nbytes)

    B_CAM = np.ndarray(CAM.shape, dtype=CAM.dtype, buffer=shm.buf)
    # B_CAM = np.zeros_like(CAM, dtype=CAM.dtype)
    print(B_CAM.shape)
    B_CAM[:] = CAM[:]
    logger.info('B_CAM: '+', '.join([str(B_CAM[ib,-1]) for ib in range(N)]))
    logger.info('CAM (in creation): '+', '.join(CAM.astype(str).reshape(-1).tolist()))

    logger.info('normal if 1 in pipeline_cam')


def write_cam(CAM,cam = [0]*8):
        CAM[:-1,:] = CAM[1:,:]
        CAM[-1,:] = cam
        B_CAM[:] = CAM[:]
        logger.info('B_CAM: '+', '.join([str(B_CAM[-1,ib]) for ib in range(8)]))
        logger.info('CAM (in write_cam): '+', '.join(CAM[-1,:].astype(str).reshape(-1).tolist()))
        logger.info('work write_cam')
        return CAM


FQ = FrameQualityEstimator()
#

def detect_target(frame,dict_calib_cam):
    """
    Args:
        frame:
        dict_calib_cam:

    Returns:

    """
    xd = 0
    yd = 0
    w2 = 1
    h2 = 1
    target_rez, target_ , segment_ = net_target.net_target_get(frame)
    try:
    	flag, _, _, anglex, r = target_rez[0]
    except:
    	flag, anglex,r = 0,0,0
    logger.info('work detect_target: '+str(flag)+','+str(anglex)+','+str(r))
    #print('**************target_rez:',target_rez)
    #print('+++++++++target_',target_)

    print('REZ: ',flag,anglex, r)
    if target_rez:
        return flag, anglex, r, {'x': xd, 'y': yd, 'w2': w2, 'h2': h2, "im_detect": flag}
    else:
        return flag, anglex, r, {'x':xd,'y':yd,'w2':w2,'h2':h2,"im_detect":None}
def trac_target(frame,detector_frame,dict_calib_cam):
    """

    Args:
        frame:
        detector_frame:
        dict_calib_cam: camer acalibrate data

    Returns:

    """
    xd = 0
    yd = 0
    w2 = 1
    h2 = 1
    logger.info('work trac_target')


    return {'x': xd, 'y': yd, 'w2': w2, 'h2': h2, "im_detect":None}

def gyro_calib(time_interval_gyro):
    calib_gyro = {"ax":0,"ay":0, "az":0,"gx":0,"gy":0, "gz":0}
    return calib_gyro
def camera_calib(frame):

    dict_FQ, flag_FQ = FQ.quality_estimation_go(frame)
    if flag_FQ:
        logger.info('good camera_calib')
        return True, dict_FQ
    else:
        logger.error('bad camera_calib')
        return False, None
#
def frame_distance(detector_frame, frame,dict_calib_cam):
    logger.info('net:')
    try:
        d,dd = net_target.net_target_get(np.array(frame).reshape(-1,frame.shape[0],frame.shape[1],frame.shape[2]))
        print("d: ",d)
        logger.info('net:'+''.join([str(dg) for dg in dd]))
        flag,_,_, anglex, r = dd
        logger.info('work frame_distance')
        return flag, anglex, r
    except:
        return 0, 0, 0

def write_point(t, dir):
    ## write frame in dir
    flag = False
    dir_im=''
    if os.path.exists(DIR_CAM):
        try:
            with open(DIR_CAM,'r') as f:
                dir_im = f.readline()
            if len(dir_im)>0:
                flag = True
                if not os.path.exists(s):
                    try:
                        os.mkdir(s)
                    except:
                        pass
            else:
                logger.error('no dir_im in write_point')
                return False, ''
        except:
            logger.error('error in write_point')
            return False,''
    logger.info('work write_point')
    return flag,dir_im

def to_control(data):
    """

    Args:
        detector_frame: detector coord
        data: image data

    Returns:

    """
    #
    global CAM
    print('*************',data)
    flag, anglex_c,  r_c = data

    new_CAM = [time.time(), 1.0, flag, anglex_c, r_c, 1.0, 0.0, 0.0]#####??????????????????
    logger.info('work to_control (in to_control0)' + ''.join([str(anglex_c), str(flag)]))
    try:
        logger.info('CAM (in to_control1): ' + ', '.join(CAM.astype(str).reshape(-1).tolist()))
        CAM = write_cam(CAM, cam=new_CAM)
        logger.info('CAM (in to_control2): ' + ', '.join(CAM.astype(str).reshape(-1).tolist()))
        success = True
    except:
        success = False
    logger.info('success (in to_control3): ' + str(success))
    logger.info('work to_control (in to_control4)'+''.join([str(anglex_c),str(flag)]))
    return success
#

detect = 1

# if not config.JETSON:
# 	cap = cv2.VideoCapture(0)
# else:
# 	cap = cv2.VideoCapture("v4l2src device=/dev/video0 ! video/x-raw, width=640, height=480 ! videoconvert ! video/x-raw,format=BGR ! appsink")
if not config.JETSON:
    path = 'new1_16_05output_video1684249697.6008341.mp4'
    # path = 'new4_14_05output_video1684248836.041548.mp4'
    path = 'new_lR5_16_0output_video1684250794.7607636.mp4'
    # path = 'new2_16_05output_video1684249940.2748854.mp4'
    # path = 'new3_16_05output_video1684250242.297106.mp4'
    path = 'nnoutput_video1684424367.8237174.mp4'
    path = 'nnoutput_video1684427627.4782228.mp4'
    # cap = cv2.VideoCapture('/home/mariya/PycharmProjects/base/video_cam/' + path)
    print('camera')
    cap = cv2.VideoCapture(0)
else:
    cap = cv2.VideoCapture( "/dev/video-cam")
# "v4l2src device=/dev/video-cam ! video/x-raw, width=640, height=480 ! videoconvert ! video/x-raw,format=BGR ! appsink"
# Check if the webcam is opened correctly
if not cap.isOpened():
    logger.info('no cam:')
    raise IOError("Cannot open webcam")
# calibrate image
while True:
    logger.info('frame')
    ret, frame = cap.read()
    frame = cv2.resize(frame, None, fx=0.5, fy=0.5, interpolation=cv2.INTER_AREA)
    logger.info('frame: Ok')

    #print(frame.shape)
    time.sleep(0.5)

    dict_calib_cam = camera_calib(frame)
    if not config.JETSON:
        cv2.namedWindow("demo", cv2.WINDOW_AUTOSIZE)
        cv2.imshow('calib', frame)
        time.sleep(1.5)


    if not dict_calib_cam[0]:
        CAM = write_cam(CAM,cam = [0]*8)
        B_CAM[:] = CAM[:]

        logger.info('B_CAM: '+', '.join([str(B_CAM[ib,-1]) for ib in range(N)]))
        logger.info('CAM (in !dict_calib_cam): '+', '.join(CAM.astype(str).reshape(-1).tolist()))
    else:
        #print(dict_calib_cam)
        cam = np.array([time.time(),dict_calib_cam[1]['brightness'][-1],0,0,0,0,dict_calib_cam[0],0])
        CAM = write_cam(CAM,cam = cam)
        B_CAM[:] = CAM[:]
        logger.info('B_CAM: '+', '.join([str(B_CAM[ib,-1]) for ib in range(N)]))
        logger.info('CAM (in dict_calib_cam): '+', '.join(CAM.astype(str).reshape(-1).tolist()))
        break
logger.info('calibr end')
print('calibr end')

#

if not config.JETSON:
        cv2.namedWindow("demo2", cv2.WINDOW_AUTOSIZE)
n_ret = 0

detect =  1
while True:
#k = 0
#while k<10:
        #k += 1
        if 1:#try:
            #
            logger.info('frame new: ' + str())
            ret, frame = cap.read()

            if ret:
                frame = cv2.resize(frame, None, fx=0.5, fy=0.5, interpolation=cv2.INTER_AREA)
                logger.info('frame: Ok')
                n_ret = 0
            else:
                logger.info('frame: no ok')
                if n_ret > 10:
                    break
                else:
                    n_ret += 1
                continue

            f_write, dir_im = write_point(0, DIR_CAM)

            if f_write:
                cv2.imwrite(dir_im+'/'+str(time.time())+'.jpg', frame)

            # detect
            if detect:
                logger.info('frame detect_target: ')
                t1 = time.time()
                detector_frame = detect_target(frame,dict_calib_cam)
                detect, anglex, r , _ = detector_frame
                logger.info('frame detect_target end: '+str(time.time()-t1))
                detector_frame = anglex, r,[True, True]
                # new_control.kate_system_control(detector_frame,  v = 0, logger = logger )
            else:
                logger.info('frame trac_target: ')
                t1 = time.time()
                detector_frame = trac_target(frame,detector_frame,dict_calib_cam)
                logger.info('frame trac_target end: ' + str(time.time() - t1))
                detect, anglex, r = frame_distance(detect, frame, dict_calib_cam)
            logger.info('frame frame_distance: ')
            t1 = time.time()


            #detect, anglex, r = frame_distance(detect, frame,dict_calib_cam)
            logger.info('frame frame_distance end: '+str(time.time()-t1))
            #print("rez :",detect,anglex,r)
            logger.info('frame: '+str(detect)+','+str(anglex)+','+str(r))
            detect = 1
            data = (detect,anglex, r)


            if not to_control(data):
                print('error')

            #if not config.JETSON:
            #    cv2.imshow('Input', frame)
        #except:
        #    print('error cam')



try:
	cap.release()
	logger.info('frame:releas ')
except:
	logger.error('cam release error frame: ')
if not config.JETSON:
        cv2.destroyAllWindows()
shm.close()
shm.unlink()
