import configparser

import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')

PATH_MAIN = '/home/jetson/motomul/'
#PATH_MAIN = '/home/mariya/PycharmProjects/base/'
#PATH_MAIN = '/home/mariya/PycharmProjects/motomul/'

def config_jetson(name  = PATH_MAIN+'config.ini'):
    config = configparser.ConfigParser()  #
    config.read(name)  #
    print(config)
    return config["MAIN"]["jetson"], config["MAIN"]['MOVING']

def confir_read(name  =  PATH_MAIN+"config.ini"):
    # parse arguments
    config = configparser.ConfigParser()  # 
    config.read(name)  # 

    temp_dir = config["MAIN"]["temp"]  #
    N_BINS = int(config["MAIN"]["N_BINS"])  #
    N_CAM = int(config["MAIN"]["N_CAM"] ) #
    time_calib_cam = int(config["Calib"]["time_cam"])
    time_calib_gyro = int(config["Calib"]["time_gyro"])
    time_interval_detect = int(config["Detector"]["time"])#
    image_size_detect = int(config["Detector"]["image_size"] ) #
    name_detect_net = config["Detector"]["name"]

    time_interval_gyro =int( config["Gyro"]["time"])
    ARDUINO_DIR = config["ARDUINO"]["ARDUINO_DIR"]
    ARDUINO_TIMEOUT = float(config["ARDUINO"]["ARDUINO_TIMEOUT"])
    ARDUINO_F = int(config["ARDUINO"]["ARDUINO_F"])
    ARDUINO_ROTATE = float(config["ARDUINO"]["ARDUINO_ROTATE"])
    ARDUINO_WAIT = float(config["ARDUINO"]["ARDUINO_WAIT"])
    ARDUINO_START = float(config["ARDUINO"]["ARDUINO_START"])

    a_x_r = config["COMAND"]["a_x_r"]
    a_x_l = config["COMAND"]["a_x_l"]
    a_x_0 = config["COMAND"]["a_x_0"]

    r_far = config["COMAND"]["r_far"]
    r_near = config["COMAND"]["r_near"]
    r_0 = config["COMAND"]["r_0"]

    dir_cam = config["CAMERA"]["DIR_CAM"]
    dir_ab = config["CAMERA"]["DIR_AB"]
    dir_ba = config["CAMERA"]["DIR_BA"]
    dir_calib = config["CAMERA"]["DIR_CALIB"]
    dir_a = config["CAMERA"]["DIR_A"]
    dir_b = config["CAMERA"]["DIR_B"]
    dir_a1 = config["CAMERA"]["DIR_A1"]

    return {"DIR_CALIB":dir_calib,"DIR_BA":dir_ba,"DIR_AB":dir_ab,
            "DIR_A":dir_a,"DIR_B":dir_b,"DIR_A1":dir_a1,"DIR_CAM":dir_cam , 
            "a_x_r":a_x_r,"a_x_l":a_x_l,"a_x_0":a_x_0,"r_far":r_far,"r_near":r_near,"r_0":r_0,
            "ARDUINO_DIR":ARDUINO_DIR,"ARDUINO_TIMEOUT":ARDUINO_TIMEOUT,"ARDUINO_F":ARDUINO_F,
            'ARDUINO_START':ARDUINO_START,"ARDUINO_ROTATE":ARDUINO_ROTATE,"ARDUINO_WAIT":ARDUINO_WAIT,
            "temp_dir":temp_dir,"time_calib_cam":time_calib_cam,"time_calib_gyro":time_calib_gyro,
            "time_interval_detect":time_interval_detect,
            "image_size_detect":image_size_detect,"name_detect_net":name_detect_net,
            "time_interval_gyro":time_interval_gyro,
            "N_BINS":N_BINS,"N_CAM":N_CAM}

MOVING = 1
dd =  config_jetson()
JETSON, MOVING = int(dd[0]), int(dd[1])
logger.info('mode jetson:'+str(JETSON)+', mode moving:'+str(MOVING))
import os

def get_var_from_env(par_key, default_val, type_val):
    out_val = os.environ.get(par_key, default_val)
    if type_val == 'int':
        return int(out_val)
    elif type_val == 'float':
        return float(out_val)
    elif type_val == 'bool':
        return bool(out_val)
    else:
        return str(out_val)


FACE_QUALITY_LIMITS = {
    # position
    'brightness': [get_var_from_env('brightness_MIN', 0.2, 'float'), get_var_from_env('brightness_MAX', 0.75, 'float')],
    # 'contrast': [get_var_from_env('contrast_MIN', 0.0, 'float'), get_var_from_env('contrast_MAX', 1.0, 'float')],
    'contrast': [get_var_from_env('contrast_MIN', 0.05, 'float'), get_var_from_env('contrast_MAX', 0.5, 'float')],
    # 'focus': [get_var_from_env('focus_MIN', 1, 'float'), get_var_from_env('focus_MAX', 700, 'float')],
    'focus': [get_var_from_env('focus_MIN', 10, 'float'), get_var_from_env('focus_MAX', 5000, 'float')],
    'illumination': [get_var_from_env('illumination_MIN', 0.3, 'float'), get_var_from_env('illumination_MAX', 1.0,
                                                                                          'float')],
    # brightness like
    'sharpness': [get_var_from_env('sharpness_MIN', 0.01, 'float'), get_var_from_env('sharpness_MAX', 10000, 'float')],

    # 'left_eye_blink': [-0.5, 0.5],
    # 'right_eye_blink': [-0.5, 0.5],
}

ARDUINO_WORK = 1
TRAIN = 0
DEPTH_CONST = 10000 ## min size border
CRITIC_DEPTH = 20 #min distance from net ******** redaction
