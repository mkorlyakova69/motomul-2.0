#include <Ultrasonic.h>
#include <NewPing.h>
#define MAX_DISTANCE 600

int TRIGGER_PIN[] = {3,5,7,9,11,13}; //пины для работы с узд
int ECHO_PIN[] = {2,4,6,8,10,12};
int j;
float tempval1[]={0,0,0,0,0,0};
float tempval2[]={0,0,0,0,0,0};
int finalval[]={0,0,0,0,0,0};
int a = 2;
int RELE_arr[] = {0,0,0,0,0,0};
#define OSI A5 //пин управления реле


void setup() {
  Serial.begin(9600);
  pinMode(OSI,OUTPUT); 
  for(int i=0; i<6; i++){
    pinMode(TRIGGER_PIN[i],OUTPUT);
    pinMode(ECHO_PIN[i],INPUT);
  }
  pinMode(OSI,OUTPUT);
}

void loop() {
//читаем порт Uart-от Mega 2560
if (Serial.available() > 0) {a = Serial.read();} 

// цикл опроса всех датчиков
  for(int i=0; i<6; i++){
    //Serial.print(String(i+1)+" Ping: ");
    NewPing sonar(TRIGGER_PIN[i], ECHO_PIN[i], MAX_DISTANCE);   
    int iterations = 1;  
    digitalWrite(TRIGGER_PIN[i],LOW);
    delay(20);
    tempval1[i] = ((sonar.ping_median(iterations) / 2) * 0.0343);
    
   
    if(tempval1[i]<35){
      iterations = 1;
      tempval1[i] = ((sonar.ping_median(iterations) / 2) * 0.0343);
    }
    //если расстояние =0  
    if (tempval1[i] == 0){tempval1[i] = 81;}
    tempval1[i] = tempval1[i] - 40;
    if (tempval1[i] < 40){RELE_arr[i] = 1;}else{RELE_arr[i] = 0;}
  }
// принятие решения в зависимости от сигнала mega2560 (если !=2 -> УЗД принимают решения <остановка, движение>)
  if (a != 2){
    if ((RELE_arr[0]+RELE_arr[1]+RELE_arr[2]+RELE_arr[3]+RELE_arr[4]+RELE_arr[5]) < 1){
      digitalWrite(OSI,LOW); //моторы работают, цепь замкнута
      //Serial.println("Уровень на реле 1");// индикация состояния реле(1,HIGH)
    }
    else {
      digitalWrite(OSI,HIGH);//моторы неработают, цепь разомкнута
      //Serial.println("Уровень на реле 0");// индикация состояния реле(0,LOw)
    }
    Serial.println("$" + String(1) + " " + String(tempval1[0])+" "+String(tempval1[1])+" "+String(tempval1[2])+" "+String(tempval1[3])+" "+String(tempval1[4])+" "+String(tempval1[5])+";");
  }
  

// принятие решения в зависимости от сигнала mega2560 (если ==2 -> УЗД непринимают решения)
  else {
    Serial.println("$" + String(0) + " " + String(tempval1[0])+" "+String(tempval1[1])+" "+String(tempval1[2])+" "+String(tempval1[3])+" "+String(tempval1[4])+" "+String(tempval1[5])+";");
    digitalWrite(OSI,LOW); //моторы работают, цепь замкнута
    //Serial.println("Уровень на реле 1");// индикация состояния реле(1,HIGH)      
  }
}
