# This is main multiprocess
import multiprocessing
import subprocess
import time
import config

import configparser

if 1:
    dict_conf = config.confir_read()
    # parse arguments
    temp_dir = dict_conf["temp_dir"]  #
    time_calib_cam = dict_conf["time_calib_cam"]
    time_calib_gyro = dict_conf["time_calib_gyro"]
    time_interval_detect = dict_conf["time_interval_detect"]#
    image_size_detect = dict_conf["image_size_detect"]  #
    name_detect_net = dict_conf["name_detect_net"]

    time_interval_gyro =dict_conf["time_interval_gyro"]

time.sleep(1)
path = config.PATH_MAIN

def func(name_func):
    print('start '+ name_func[1])
    time.sleep(10)
    subprocess.run(name_func)
    print('stop main'+ name_func[1])

list_func = [["python3", path+"main_pipeline_cam.py"],["python3",  path+"main_pipeline_bins.py"],["python3", path+"main_pipeline.py"]]
#list_func = [["python3", path+"test1.py"],["python3",  path+"test2.py"],["python3", "main_pipeline.py"]]
#list_func = [["python3", path+"main_pipeline_cam.py"],["python3", path+"main_pipeline1.py"]]
if __name__ == '__main__':

    with multiprocessing.Pool() as p:
        p.map(func, list_func)
    print('done')




