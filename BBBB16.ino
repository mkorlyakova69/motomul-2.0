#include <VescUart.h>

#include <PinChangeInterrupt.h>
#include <PinChangeInterruptBoards.h>
#include <PinChangeInterruptPins.h>
#include <PinChangeInterruptSettings.h>

// #include <EncButton.h>
// #include <EncButton2.h>
#include "GyverButton.h"
//#include <GyverTimers.h>
//#include <GParser.h>
//#include <parseUtils.h>
//#include <unicode.h>
//#include <url.h>
//#include <AsyncStream.h>

#include <Mapf.h>
#include <Button.h>

// указываем обработчик, терминатор (символ конца приёма) и таймаут в мс
// в <> указан размер буфера!
//AsyncStream<100> serial1(&Serial, ';');  // СЕриал для Джетс
//AsyncStream<100> serial2(&Serial3, ';'); // Ссериал для УЗД

VescUart CH2_Uart; // драйвер правого привода CH2
VescUart CH3_Uart; // драйвер левого привода CH3

const int buttF2 = 60; // переключатель "Фара 2"
const int buttF1 = 61; // переключатель "Фара 1"

const int BU = 63; // Вход кнопки движения вперёд A9 (PCINT17)
const int BD = 62; // Вход кнопки движения назад A8 (PCINT16)
const int BL = 64; // Вход кнопки движения влево A10 (PCINT18)
const int BR = 65; // Вход кнопки движения вправо A11 (PCINT19)

const int butAct_down = 66; // Вход переключателя актуатор вниз A12 (PCINT20)
const int butAct_up = 67;   // Вход переключателя актуатор вверх A13 (PCINT21)

const int USD_off = 68; // Вход переключателя Отключить УЗД А14 (PCINT22)

const int Ru_M = 69; // Вход переключателя ручное управление/ движение за меткой А15 (PCINT23)

const int bA = 53; // вход кнопки А
const int bB = 52; // вход кнопки Б

const int bStop = 21; // кнопка СТОП (INT0)

const int OffUzd = 68;  //переключатель "Отключить УЗД"

#define MAX_SPEED A1 // аналоговый вход переменного резистора на макс скорость

// номер контакта для подключения индикатора
const int indA = 6;       // Индикатор "А"
const int indB = 7;       // Индикатор "Б"
const int indM = 8;       // Индикатор "М"
const int indBat25 = 45;  // Индикатор "Батарея 25%"
const int indBat50 = 44;  // Индикатор "Батарея 50%"
const int indBat75 = 43;  // Индикатор "Батарея 75%"
const int indBat100 = 42; // Индикатор "Батарея 100%"
const int zum = 4;        // "Зуммер"
const int indPrM = 5;     // "Проблесковый маячок"
const int indF1 = 2;      // "ФАРА 1"
const int indF2 = 3;      // "ФАРА 2"
const int akt_up = 10;    // "АКТУАТОР ВВЕРХ"
const int akt_down = 50;  // "АКТУАТОР ВНИЗ"
const int klax = 20;      // "КЛАКСОН"

volatile bool butBU = false; // флаги нажатия вперед, назад, влево, вправо
volatile bool butBD = false;
volatile bool butBL = false;
volatile bool butBR = false;

// volatile bool butStop = false; // флаг стоп

// volatile bool butRuM = false; //флаг нажатия тумблера

volatile int counter = 0;

// инициализациы кнопок А и Б через библиотеку
GButton buttBU(BU);
GButton buttBD(BD);
GButton buttBL(BL);
GButton buttBR(BR);

GButton butA(bA);
GButton butB(bB);

////////////////////////////////////////////////////

void setup() {
  Serial.begin(115200); // Сериал в комп serial0 считывать раз в полсекунды!!!!
  while (!Serial) {;}
  Serial3.begin(9600); // сериал 3 к узд serial1

  Serial1.begin(115200); // драйвер правого привода serial1
  while (!Serial1) {;}
  Serial2.begin(115200); // драйвер левого привода serial 2
  while (!Serial2) {;}

  CH2_Uart.setSerialPort(&Serial1);
  CH3_Uart.setSerialPort(&Serial2);

  buttBU.setDebounce(100);
  buttBU.setTimeout(300);
  // buttBU.setType(HIGH_PULL);
  // buttBU.setDirection(NORM_CLOSE);

  buttBD.setDebounce(100);
  buttBD.setTimeout(300);
  // buttBD.setType(HIGH_PULL);
  // buttBD.setDirection(NORM_CLOSE);

  buttBL.setDebounce(100);
  buttBL.setTimeout(300);
  // buttBL.setType(HIGH_PULL);
  // buttBL.setDirection(NORM_CLOSE);

  buttBR.setDebounce(100);
  buttBR.setTimeout(300);
  // buttBR.setType(HIGH_PULL);
  // buttBR.setDirection(NORM_CLOSE);

  butA.setDebounce(100);
  butA.setTimeout(1000);
  // butA.setType(HIGH_PULL);
  // butA.setDirection(NORM_CLOSE);

  butB.setDebounce(100);
  butB.setTimeout(300);
  butB.setType(LOW_PULL);
  butB.setDirection(NORM_CLOSE);

  // прерывание СТОП аппаратное
  // attachInterrupt(2, stop_f, FALLING);

  // прерывание с PCINT вперед назад влево вправо
  attachPinChangeInterrupt(digitalPinToPCINT(BU), butBU_f, FALLING);
  attachPinChangeInterrupt(digitalPinToPCINT(BD), butBD_f, FALLING);
  attachPinChangeInterrupt(digitalPinToPCINT(BL), butBL_f, FALLING);
  attachPinChangeInterrupt(digitalPinToPCINT(BR), butBR_f, FALLING);

  // attachPinChangeInterrupt(digitalPinToPCINT(Ru_M), butRuM_f, FALLING);

  // прерывание по таймеру для получения и отправки в jets
  // Timer3.setPeriod(500); //раз в полсекунды проверяет буфер порта с компьютера
  // Timer3.enableISR(CHANNEL_A);

  // прерывание по таймеру для получения данных с УЗД
  // Timer4.setPeriod(250); //каждые 250 мс проверять буфер порта от узд
  // Timer4.enableISR(CHANNEL_A);

  pinMode(bStop, INPUT);

  pinMode(buttF1, INPUT);
  pinMode(buttF2, INPUT);

  pinMode(butAct_up, INPUT);
  pinMode(butAct_down, INPUT);

  // индикаторы, инициализация и задание начального состояния
  pinMode(indA, OUTPUT);
  pinMode(indB, OUTPUT);
  pinMode(indM, OUTPUT);
  pinMode(indBat25, OUTPUT);
  pinMode(indBat50, OUTPUT);
  pinMode(indBat75, OUTPUT);
  pinMode(indBat100, OUTPUT);
  pinMode(zum, OUTPUT);
  pinMode(indPrM, OUTPUT);
  pinMode(indF1, OUTPUT);
  pinMode(indF2, OUTPUT);
  pinMode(akt_up, OUTPUT);
  pinMode(akt_down, OUTPUT);
  pinMode(klax, OUTPUT);

  digitalWrite(indA, HIGH);
  digitalWrite(indB, HIGH);
  digitalWrite(indM, HIGH);
  digitalWrite(indBat25, LOW);
  digitalWrite(indBat50, LOW);
  digitalWrite(indBat75, LOW);
  digitalWrite(indBat100, LOW);
  digitalWrite(zum, HIGH);
  digitalWrite(indPrM, HIGH);
  digitalWrite(indF1, HIGH);
  digitalWrite(indF2, HIGH);
  digitalWrite(akt_up, HIGH);
  digitalWrite(akt_down, HIGH);
  digitalWrite(klax, HIGH);
}

///////////////////////////////////////////////////
#define PARSE_AMOUNT_UZD 7 // число значений в массиве, который хотим получить
#define PARSE_AMOUNT_JETS 3

int UZD[PARSE_AMOUNT_UZD]; // массив численных значений после парсинга
int jets[PARSE_AMOUNT_JETS];

boolean recievedFlag;
boolean getStarted;
byte index;
String string_convert = "";

void parsing_UZD() {
  if (Serial3.available() > 0) {
    char incomingByte = Serial3.read(); // обязательно ЧИТАЕМ входящий символ
    if (getStarted) { // если приняли начальный символ (парсинг разрешён)
      if (incomingByte != ' ' && incomingByte != ';') {                                 // если это не пробел И не конец
        string_convert += incomingByte; // складываем в строку
      } else {                                      // если это пробел или ; конец пакета
        UZD[index] = string_convert.toInt(); // преобразуем строку в int и кладём в массив
        string_convert = "";                 // очищаем строку
        index++;                             // переходим к парсингу следующего элемента массива
      }
    }
    if (incomingByte == '$') {                      // если это $
      getStarted = true;   // поднимаем флаг, что можно парсить
      index = 0;           // сбрасываем индекс
      string_convert = ""; // очищаем строку
    }
    if (incomingByte == ';') {                      // если таки приняли ; - конец парсинга
      getStarted = false;  // сброс
      recievedFlag = true; // флаг на принятие
    }
  }
}

void parsingJets() {
  if (Serial.available() > 0) {
    char incomingByte = Serial.read(); // обязательно ЧИТАЕМ входящий символ
    if (getStarted) { // если приняли начальный символ (парсинг разрешён)
      if (incomingByte != ' ' && incomingByte != ';') {                                 // если это не пробел И не конец
        string_convert += incomingByte; // складываем в строку
      } else {                                       // если это пробел или ; конец пакета
        jets[index] = string_convert.toInt(); // преобразуем строку в int и кладём в массив
        string_convert = "";                  // очищаем строку
        index++;                              // переходим к парсингу следующего элемента массива
      }
    }
    if (incomingByte == '$') {                      // если это $
      getStarted = true;   // поднимаем флаг, что можно парсить
      index = 0;           // сбрасываем индекс
      string_convert = ""; // очищаем строку
    }
    if (incomingByte == ';') {                      // если таки приняли ; - конец парсинга
      getStarted = false;  // сброс
      recievedFlag = true; // флаг на принятие
    }
  }
}
void parsing_stop(){
  while(Serial.available()) {
    Serial.read();
    delay(5);
  }  
}

// volatile uint32_t debounce;    //для устранения дребезга

/////////////////////////////////////////////////////////
// функции для прерывания с PCINT
void butBU_f(void) {
  butBU = true;
}
void butBD_f(void) {
  butBD = true;
}
void butBL_f(void) {
  butBL = true;
}
void butBR_f(void) {
  butBR = true;
}

//////////////////////////////////////////////////////////
// функция перевода скорости в положительное значение (для вперед и остального)
void fPos_speed(double &speed, double &speedStep, double &max_speed) {
  if (speed < 0) speed = speed * (-1.0);
  if (speedStep < 0) speedStep = speedStep * (-1.0);
  if (max_speed < 0) max_speed = max_speed * (-1.0);

  if (speed < max_speed) {
    speed += speedStep;
  } else speed = max_speed;
}

// функция перевода скорости в отрцательное значение (для назад)
void fNeg_speed(double &speed, double &speedStep, double &max_speed) {
  if (speed > 0) speed = speed * (-1.0);
  if (speedStep > 0) speedStep = speedStep * (-1.0);
  if (max_speed > 0) max_speed = max_speed * (-1.0);

  if (speed > max_speed) {
    speed += speedStep;
  } else speed = max_speed;
}

// функция движения с кнопок
void butMov(double &speedH_2, double &speedH_3, double &speedStep) {
  //speedStep = 0.0002;
  //actuator;
  if (!digitalRead(butAct_down)) {
    digitalWrite(akt_down, LOW);
  } else digitalWrite(akt_down, HIGH);
    
  buttBU.tick();
  buttBD.tick();
  buttBL.tick();
  buttBR.tick();
  double var = 0.0;

  double max_speed = mapf(analogRead(MAX_SPEED), 0, 1023, 0.0, 1.0);
  // double max_speed = 0.5;

  // if(buttBU.isHold()){
  if (buttBU.isHold() && !buttBL.isHold() && !buttBR.isHold() && !buttBD.isHold()) {
    // прямо
    fPos_speed(speedH_2, speedStep, max_speed);
    speedH_3 = speedH_2;
  } else if (buttBD.isHold() && !buttBU.isHold() && !buttBL.isHold() && !buttBR.isHold()) {
    // назад
    fNeg_speed(speedH_2, speedStep, max_speed);
    var = speedH_2;
    speedH_3 = var;
  } else if (buttBL.isHold() && !buttBD.isHold() && !buttBU.isHold() && !buttBR.isHold()) {
    // влево
    // ch2
    fPos_speed(speedH_3, speedStep, max_speed);
    var = speedH_3;
    speedH_3 = var;
    //speedH_2 = var / 2 * (1);
    speedH_2 = 0;
  } else if (buttBR.isHold() && !buttBD.isHold() && !buttBU.isHold() && !buttBL.isHold()) {
    // вправо
    // ch3
    fPos_speed(speedH_2, speedStep, max_speed);
    var = speedH_2;
    speedH_2 = var;
    //speedH_3 = var / 2 * (1);
    speedH_3 = 0;
  } else if (buttBU.isHold() && buttBL.isHold()) {
    // вперед и лево
    fPos_speed(speedH_3, speedStep, max_speed);
    var = speedH_3;
    speedH_3 = var;
    speedH_2 = var / 4;
  } else if (buttBU.isHold() && buttBR.isHold()) {
    // вперед и право
    fPos_speed(speedH_2, speedStep, max_speed);
    var = speedH_2;
    speedH_2 = var;
    speedH_3 = var / 4;
  } else if (speedH_2 > 0 || speedH_3 > 0) {
    //speedStep = 0.05;
    //speedH_2 -= speedStep;
    //speedH_3 -= speedStep;

    speedH_2 -= 0.001 ;
    speedH_3 -= 0.001;

    if (speedH_2 < 0) speedH_2 = 0;
    if (speedH_3 < 0) speedH_3 = 0;
  } else if (speedH_2 < 0 || speedH_3 < 0) {
    //speedStep = 0.05;                       //изменение шага на торможение
    //speedH_2 -= speedStep;
    //speedH_3 -= speedStep;

    speedH_2 += 0.001 ;
    speedH_3 += 0.001;

    if (speedH_2 > 0) speedH_2 = 0;
    if (speedH_3 > 0) speedH_3 = 0;
  }
  CH2_Uart.setDuty(speedH_2); // левый
  CH3_Uart.setDuty(-speedH_3);  // правый
}

//////////////////////////////////////////////////////////
// Функции движения от Jets
void fSlowDown(double &speedJet, double &speedStep, double &max_speed) {
  if (speedJet > max_speed) {
    speedJet -= speedStep;
  }
}

////ПОЛОВИНА СКОРОСТИ ПРЯМО
void jetMove_1(double &speedJet, double &speedStep) {
  double max_speed = 0.25;
  if (speedJet > max_speed) {
    fSlowDown(speedJet, speedStep, max_speed);
  } else fPos_speed(speedJet, speedStep, max_speed);

  CH2_Uart.setDuty(speedJet);
  CH3_Uart.setDuty(-speedJet);
}

////ПОЛОВИНА СКОРОСТИ НА ПОВОРОТ ВПРАВО
void jetMove_1R(double &speedJet, double &speedStep) {
  double max_speed = 0.25;
  if (speedJet > max_speed) {
    fSlowDown(speedJet, speedStep, max_speed);
  } else fPos_speed(speedJet, speedStep, max_speed);

  //speedJet = speedJet * (-1);
  CH2_Uart.setDuty(speedJet);
  CH3_Uart.setDuty(speedJet);
}

////ПОЛОВИНА СКОРОСТИ НА ПОВОРОТ ВЛЕВО
void jetMove_1L(double &speedJet, double &speedStep) {
  double max_speed = 0.25;
  if (speedJet > max_speed) {
    fSlowDown(speedJet, speedStep, max_speed);
  } else fPos_speed(speedJet, speedStep, max_speed);

  speedJet = speedJet * (-1);
  CH2_Uart.setDuty(speedJet);
  CH3_Uart.setDuty(speedJet);
}
// ПОЛНАЯ СКОРОСТЬ ПРЯМО
void jetMove_2(double &speedJet, double &speedStep) {
  double max_speed = 0.5;
  fPos_speed(speedJet, speedStep, max_speed);

  CH2_Uart.setDuty(speedJet);
  CH3_Uart.setDuty(-speedJet);
}

// ПОЛНАЯ СКОРОСТЬ НА ПОВОРОТ ВПРАВО
void jetMove_2R(double &speedJet, double &speedStep) {
  double max_speed = 0.5;
  fPos_speed(speedJet, speedStep, max_speed);
  //speedJet = speedJet * (-1);

  CH2_Uart.setDuty(speedJet);
  CH3_Uart.setDuty(speedJet);
}

// ПОЛНАЯ СКОРОСТЬ НА ПОВОРОТ ВЛЕВО
void jetMove_2L(double &speedJet, double &speedStep) {
  double max_speed = 0.5;
  fPos_speed(speedJet, speedStep, max_speed);

  speedJet = speedJet * (-1);
  CH2_Uart.setDuty(speedJet);
  CH3_Uart.setDuty(speedJet);
}
// СТОИМ
void jetMove_0(double &speedJet, double &speedStep) {
  if (speedJet > 0) {
    speedJet -= speedStep;
  } else if (speedJet < 0) {
    speedJet += speedStep;
  }
  CH2_Uart.setDuty(speedJet);
  CH3_Uart.setDuty(-speedJet);
}
//СТОИМ ПОСЛЕ УГЛА
void jetMove_0angle(double &speedJet, double &speedStep) {
  if (speedJet > 0) {
    speedJet -= speedStep;
  } else if (speedJet < 0) {
    speedJet += speedStep;
  }
  CH2_Uart.setDuty(speedJet);
  CH3_Uart.setDuty(speedJet);
}
// ПОВОРОТЫ
// НА СКОРОСТИ 1
int jetMove_angle_1(double &speedJet, double &speedStep, int angle) {
  // uint32_t current_time = millis();
  uint32_t first_time = millis();

  if (angle == 10) {
    while (millis() - first_time <= 1465) {
      jetMove_1R(speedJet, speedStep);
      // Serial.println(millis() - first_time);
      // Serial.println("BBBB");
    }
  } else if (angle == -10) {
    while (millis() - first_time <= 1466) {
      jetMove_1L(speedJet, speedStep);
    }
  }

  if (angle == 20) {
    while (millis() - first_time <= 2074) {
      jetMove_1R(speedJet, speedStep);
    }
  } else if (angle == -20) {
    while (millis() - first_time <= 2074) {
      jetMove_1L(speedJet, speedStep);
    }
  }

  if (angle == 30) {
    while (millis() - first_time <= 2611) {
      jetMove_1R(speedJet, speedStep);
    }
  } else if (angle == -30) {
    while (millis() - first_time <= 2611) {
      jetMove_1L(speedJet, speedStep);
    }
  }

  if (angle == 40) {
    while (millis() - first_time <= 3148) {
      jetMove_1R(speedJet, speedStep);
    }
  } else if (angle == -40) {
    while (millis() - first_time <= 3148) {
      jetMove_1L(speedJet, speedStep);
    }
  }

  if (angle == 50) {
    while (millis() - first_time <= 3685) {
      jetMove_1R(speedJet, speedStep);
    }
  } else if (angle == -50) {
    while (millis() - first_time <= 3685) {
      jetMove_1L(speedJet, speedStep);
    }
  }

  if (angle == 60) {
    while (millis() - first_time <= 4222) {
      jetMove_1R(speedJet, speedStep);
    }
  } else if (angle == -60) {
    while (millis() - first_time <= 4222) {
      jetMove_1L(speedJet, speedStep);
    }
  }

  if (angle == 70) {
    while (millis() - first_time <= 4759) {
      jetMove_1R(speedJet, speedStep);
    }
  } else if (angle == -70) {
    while (millis() - first_time <= 4759) {
      jetMove_1L(speedJet, speedStep);
    }
  }

  if (angle == 80) {
    while (millis() - first_time <= 5296) {
      jetMove_1R(speedJet, speedStep);
    }
  } else if (angle == -80) {
    while (millis() - first_time <= 5296)
    {
      jetMove_1L(speedJet, speedStep);
    }
  }

  if (angle == 90) {
    while (millis() - first_time <= 5833) {
      jetMove_1R(speedJet, speedStep);
    }
  } else if (angle == -90) {
    while (millis() - first_time <= 5833) {
      jetMove_1L(speedJet, speedStep);
    }
  }
  // Serial.println("AAAA");
}
// НА СКОРОСТИ 2
void jetMove_angle_2(double &speedJet, double &speedStep, int angle) {
  uint32_t first_time = millis();
  if (angle == 10) {
    while (millis() - first_time <= 965) {
      jetMove_2R(speedJet, speedStep);
    }
  } else if (angle == -10) {
    while (millis() - first_time <= 965) {
      jetMove_2L(speedJet, speedStep);
    }
  }

  if (angle == 20) {
    while (millis() - first_time <= 1364) {
      jetMove_2R(speedJet, speedStep);
    }
  } else if (angle == -20) {
    while (millis() - first_time <= 1364) {
      jetMove_2L(speedJet, speedStep);
    }
  }

  if (angle == 30) {
    while (millis() - first_time <= 1671) {
      jetMove_2R(speedJet, speedStep);
    }
  } else if (angle == -30) {
    while (millis() - first_time <= 1671) {
      jetMove_2L(speedJet, speedStep);
    }
  }

  if (angle == 40) {
    while (millis() - first_time <= 1929) {
      jetMove_2R(speedJet, speedStep);
    }
  } else if (angle == -40) {
    while (millis() - first_time <= 1929) {
      jetMove_2L(speedJet, speedStep);
    }
  }

  if (angle == 50) {
    while (millis() - first_time <= 2163) {
      jetMove_2R(speedJet, speedStep);
    }
  } else if (angle == -50) {
    while (millis() - first_time <= 2163) {
      jetMove_2L(speedJet, speedStep);
    }
  }

  if (angle == 60) {
    while (millis() - first_time <= 2396) {
      jetMove_2R(speedJet, speedStep);
    }
  } else if (angle == -60) {
    while (millis() - first_time <= 2396) {
      jetMove_2L(speedJet, speedStep);
    }
  }

  if (angle == 70) {
    while (millis() - first_time <= 2628) {
      jetMove_2R(speedJet, speedStep);
    }
  } else if (angle == -70) {
    while (millis() - first_time <= 2628) {
      jetMove_2L(speedJet, speedStep);
    }
  }

  if (angle == 80) {
    while (millis() - first_time <= 2861) {
      jetMove_2R(speedJet, speedStep);
    }
  } else if (angle == -80) {
    while (millis() - first_time <= 2861) {
      jetMove_2L(speedJet, speedStep);
    }
  }

  if (angle == 90) {
    while (millis() - first_time <= 3094) {
      jetMove_2R(speedJet, speedStep);
    }
  } else if (angle == -90) {
    while (millis() - first_time <= 965) {
      jetMove_2L(speedJet, speedStep);
    }
  }
}
//////////////////////////////////////////////////////////
/*void f_move(double& speedH_2, double& speedH_3, double& speedJet, double& speedStep){
  butA.tick();
  butB.tick();

  if(!digitalRead(Ru_M)){
    //функция движения от кнопок
    butMov(speedH_2, speedH_3, speedStep);

  } else {
    parsingJets();

    if(jets[0] == 1) {
      //функция движения от компа по метке
      switch(jets[1]){
        case 1:
          jetMove_1(speedJet, speedStep);

          if(jets[3] != 0){
            jetMove_angle_1(speedJet, speedStep, jets[3]);
          }
          break;
        case 2:
          jetMove_2(speedJet, speedStep);
          jetMove_angle_2(speedJet, speedStep, jets[3]);
          break;
        default:
          jetMove_0(speedJet, speedStep);
      }
    }
    //uzd_to_jets(speedJet, speedStep);
  }
}*/

//////////////////////////////////////////////////////////
void uzd_to_jets(double& speedJet, double& speedStep) {
  parsing_UZD();
  // отключение двигателей если менее 40 см
  int uzd0 = UZD[0];
  int uzd1 = UZD[1];
  int uzd2 = UZD[2];
  int uzd3 = UZD[3];
  int uzd4 = UZD[4];
  int uzd5 = UZD[5];

  if (uzd0 < 40 || uzd1 < 40 || uzd2 < 40 || uzd3 < 40 || uzd4 < 40 || uzd5 < 40) {
    // отключение двигателей
    bool x = 0;
    //остановка двигателей
    while(!x){
      jetMove_0(speedJet, speedStep);
      if(speedJet <= 0.0) x = 1;
    }
    //Присвоение массиву команды от Джетс значения 3 0 0
    jets[0] = 3;
    jets[1] = 0;
    jets[2] = 0;
  }
  Serial.print("$6 ");
  // Jetson должен получать строку типа "$6 10 20 30 40 50 60;\n"
  for(int i = 0; i < 6; i++){
    Serial.print(uzd0);
    Serial.print(' ');
  }
  Serial.print('\n');
}
//////////////////////////////////////////////////////////
//цикл в котором происходит остановка в 3 ем модуле для включения актуатора(то есть уиеньшение скорости до 0)
void module3_stop(double& speedJet, double& speedStep){
  bool x = 0;
  //остановка двигателей
  while(!x){
    jetMove_0(speedJet, speedStep);
    if(speedJet <= 0.0) x = 1;
  }
}
/////////////////////////////////////////////////////////
//Чтение состояния переключателя УЗД и отправка команды Нане
//команда УЗД о включении реле
//"$0;" - команда: реле отключено (работа оператора)
//"$1;" - команда: реле включено (авто режим)
/*void Off_UZD(){
  if (!digitalRead(OffUzd)){
    //ВКЛЮЧИТЬ УЗД
    Serial3.println("$1;");
  } else Serial3.println("$0;");
}*/
//функция проблескового маячка
void Prob_M_on(){
  
  //digitalWrite(indPrM, HIGH);
  //delay(300);
  digitalWrite(indPrM, LOW);

}

void Prob_M_off(){
  
  digitalWrite(indPrM, HIGH);
  delay(300);
  digitalWrite(indPrM, LOW);
  delay(300);
  digitalWrite(indPrM, HIGH);

}
void uzd_off(){
  //отключи УЗД 
  while(UZD[0] == 1){
    Serial3.write(2);
    parsing_UZD();
    //Serial.println(UZD[0]);
  }
}
void uzd_on(){
  //включи УЗД 
  while(UZD[0] == 0){
    Serial3.write(3);
    parsing_UZD();
    //Serial.println(UZD[0]);
  }
}
////////////////////////////////////////////////////////
double speedH_2 = 0.0;
double speedH_3 = 0.0;

double speedJet = 0.0;

// bool jetsReady = 0;

bool flagM = 0; // флаг о загорании М(запись траектории)

bool flagA_hold = 0;
bool flagA_click = 0;
bool flagB_click = 0;

bool light_jets = 0;

bool lightF1 = 0;

bool offUzd = 0;

///////////////////////////////////////////////////////////

void loop() {
  
  // bool stop_flag = 0;
  // butStopInt.tick();

  butA.tick();
  butB.tick();

  if (butA.isHold()) flagA_hold = 1;
  if (butA.isClick()) flagA_click = 1;
  if (butB.isClick()) flagB_click = 1;

  double speedStep = 0.0002;         // шаг ускорения и замедления двигателей
  unsigned long timing;              //для ожидания
  ///////////////////////////
  parsing_UZD();
  //uzd_off();
  
  /*if(offUzd == 0) {
    //parsing_UZD();
    uzd_off();
    //Serial3.write(2);
    offUzd = 1;
    //delay(300);
    //parsing_stop();
  }*/
  //parsing_stop();

  if (butBU || butBD || butBL || butBR) {

    //Off_UZD();
    // Serial.println("butBU");
    butBU = false;
    butBD = false;
    butBL = false;
    butBR = false;

    if (!digitalRead(Ru_M)) {
      disablePCINT(digitalPinToPCINT(BU));
      disablePCINT(digitalPinToPCINT(BD));
      disablePCINT(digitalPinToPCINT(BL));
      disablePCINT(digitalPinToPCINT(BR));
      // функция движения от кнопок
      // выход по нажатию стоп
      // while(butStop == false)
      while (digitalRead(bStop)) {
        butMov(speedH_2, speedH_3, speedStep);
        //parsing_UZD();
        //Serial.println(UZD[0]);
        // Serial.println(speedH_2);
      }
      //Serial.println("aaa");
      // butStop = false;
      enablePCINT(digitalPinToPCINT(BU));
      enablePCINT(digitalPinToPCINT(BD));
      enablePCINT(digitalPinToPCINT(BL));
      enablePCINT(digitalPinToPCINT(BR));
    } else {
      //УЗД включить - 1   
      //uzd_on();

      disablePCINT(digitalPinToPCINT(BU));
      disablePCINT(digitalPinToPCINT(BD));
      disablePCINT(digitalPinToPCINT(BL));
      disablePCINT(digitalPinToPCINT(BR));
      // функция движения от компа по метке
      //Serial.println("aaaa");
      //bool exit100 = 0;
      Prob_M_on();
        while(jets[1] != 100 && jets[2] != 100){
       //while(!exit100){

         if(millis() - timing > 4000){
           timing = millis();
           // ($1,1;) - начало модуля движение за траекторией
           Serial.println("$1 1;");
         }
         /*if(jets[0] == 100 && jets[1] == 100 && jets[2] == 100){
           exit100 = 1;
         }*/
         //delay(5000);
         parsingJets();
       }

       //Serial.println(jets[0]);
       //Serial.println(jets[1]);
       //Serial.println(jets[2]);
       timing=0;
       delay(300);
       parsing_stop();
      // Serial.println(butStop);
      int i = 0;
      /*jets[0] = 0;
      jets[1] = 0;
      jets[2] = 0;*/
       //Serial.println(jets[0]);
       //Serial.println(jets[1]);
       //Serial.println(jets[2]);      

      //Prob_M_on();
      //digitalWrite(indPrM, LOW);
      //delay(300);
      //digitalWrite(indPrM, H);

      bool exit_module1 = 0;
      uzd_on();
      //Serial.println(UZD[0]);
      //Serial.println(UZD[4]);

      while (digitalRead(bStop)) {
        //parsing_UZD();
        //Serial.println(UZD[0]);

        bool stop_flag = 0;

        parsingJets();
        // Serial.println(jets[1]);
        //ФАРЫ в модуле метки
        if(jets[0] == 5 && jets[1] == 1){
          digitalWrite(indF1, LOW);
          digitalWrite(indF2, LOW);
          light_jets=1;
          while(jets[0] != 100 && jets[1] != 100 && jets[2] != 100){
            if(millis()-timing > 4000){
              timing = millis();
              Serial.println("$5 2;");
            }
            parsingJets();
          }
          timing=0;
          delay(300);
          parsing_stop();

        }else if(jets[0] == 1){

          switch (jets[1]) {
            case 1:
              i = 0;
              // половина скорости
              // проверять угол поворота и отработать поворот
              if (jets[2] == 0) {   
                jetMove_1(speedJet, speedStep);
              } else {
                while (!stop_flag) {
                  if (speedJet > 0.0) {
                    jetMove_0(speedJet, speedStep);
                    // Serial.println(speedJet);
                  } else stop_flag = 1;
                }
                jetMove_angle_1(speedJet, speedStep, jets[2]); // цикл while
                // jetMove_0angle(speedJet, speedStep);
                //jets[1] = 0;
                jets[2] = 0;
                i = 1;
              }
              break;

            case 2:
              i = 0;
              // полная скорость
              if (jets[2] == 0) {
                jetMove_2(speedJet, speedStep);
              } else {
                while (!stop_flag) {
                  if (speedJet > 0.0) {
                    jetMove_0(speedJet, speedStep);
                  } else stop_flag = 1;
                }
                jetMove_angle_2(speedJet, speedStep, jets[2]); // while
                // jetMove_0angle(speedJet, speedStep);
                i = 1;
                //jets[1] = 0;
                jets[2] = 0;
              }
              break;
        
            case 11: //запрос ручного управления при потере маяка???
              while (!stop_flag) {
                if (speedJet > 0.0) {
                  jetMove_0(speedJet, speedStep);
                  // Serial.println(speedJet);
                } else stop_flag = 1;
              }        

            default:
              // стоим
              if (i == 1) {
                jetMove_0angle(speedJet, speedStep);
              }   else jetMove_0(speedJet, speedStep);
          }
        }
        // uzd_to_jets(speedJet, speedStep);
        //if(!digitalRead(bStop)) exit_module1 = 1;

      }
      // delay(50);
      //Serial.println("aaaa");
      delay(300);
      parsing_stop();
      
      while(jets[0] != 100 && jets[1] != 100 && jets[2] != 100){
        
        if(millis()-timing > 4000){
          timing = millis();
          //($1,0)-конец модуля движения за траекторий  
          Serial.println("$1 0;");
        }
        //Serial.println("$1 0;");
        //delay(500);
        parsingJets();

      }
      timing = 0;
      delay(300);
      parsing_stop();
      butA.tick();

      uzd_off();
      //offUzd = 0;
    }
    //uzd_off();
    //offUzd = 0;
    //Serial.println(UZD[0]);
    //Serial.println(UZD[4]);
    
    butBU = 0;
    butBD = 0;
    butBL = 0;
    butBR = 0;
    //Serial.println(butBU);
    //digitalWrite(indPrM, HIGH);
    Prob_M_off();
       //Serial.println(jets[0]);
       //Serial.println(jets[1]);
       //Serial.println(jets[2]);       

    enablePCINT(digitalPinToPCINT(BU));
    enablePCINT(digitalPinToPCINT(BD));
    enablePCINT(digitalPinToPCINT(BL));
    enablePCINT(digitalPinToPCINT(BR));

  }
  ///////////////////////////
  if (flagA_hold) {
    //модуль полностью без УЗД
    //Off_UZD();
    // butA.tick();

    flagA_hold = 0;
    //Serial.println("A_HOLD");
    // зуммер
    digitalWrite(zum, LOW);
    delay(1000);
    digitalWrite(zum, HIGH);

    //Serial.println(jets[0]);
    //Serial.println(jets[1]);
    //Serial.println(jets[2]);      

    while(jets[1] != 100 && jets[2] != 100){
      if(millis()-timing > 4000){
        timing = millis();
        Serial.println("$2 1;");
      }
      //Serial.println("$2 1;"); //($2,1;)-начало модуля записи движения
      //delay(500);
      parsingJets();
    }
    
    //Serial.println("AAAAAAAAA");
    timing = 0;
    delay(300);
    parsing_stop();

    disablePCINT(digitalPinToPCINT(BU));
    disablePCINT(digitalPinToPCINT(BD));
    disablePCINT(digitalPinToPCINT(BR));
    disablePCINT(digitalPinToPCINT(BL));

    // jets[0] = 2 - запись траектории
    // jets[0] = 4 - проведение калибровки перед записью
    parsingJets();

    while(jets[1] != 666){
      //Serial.println("BBBBBB");
      parsingJets();

      switch (jets[0]) {
      // запись траектории
      case 2:
        while (jets[0] == 2 && jets[1] != 9) {
          //($2 9;) - команда о включении светодиода М, маршрут записан
          parsingJets();

          switch (jets[1]) {

          //($2 7 0;) включить А
          case 7:
            digitalWrite(indA, LOW);
            break;

          //($2 2 0;) готов вести запись
          case 2:
            butB.tick();
            digitalWrite(indA, HIGH);

            while (!butB.isClick()) {
              butMov(speedH_2, speedH_3, speedStep);
              // f_move(speedH_2, speedH_3, speedJet, speedStep);
              butB.tick();
            }
            jetMove_0(speedJet, speedStep);

            while(jets[0] != 100 && jets[1] != 100 && jets[2] != 100){
            
              if(millis()-timing > 4000){
                timing = millis();
                Serial.println("$2 88;");
              }
              parsingJets();
            }
            timing = 0;
            delay(300);
            parsing_stop();

            //Serial.println("$2 88;");
            jets[0] = 2;
            jets[1] = 0;
            jets[2] = 0;
          
            break;

          //($2 8 0;) включить Б
          case 8:
            digitalWrite(indA, HIGH);

            digitalWrite(indB, LOW);
            delay(1000);
            digitalWrite(indB, HIGH);

            digitalWrite(zum, LOW);
            delay(2000);
            digitalWrite(zum, HIGH);

            digitalWrite(zum, LOW);
            delay(500);
            digitalWrite(zum, HIGH);

            while (!butA.isClick()) {
              // f_move(speedH_2, speedH_3, speedJet, speedStep);
              butMov(speedH_2, speedH_3, speedStep);
              butA.tick();
            }

            jetMove_0(speedJet, speedStep);
            jets[1] = 0;
            jets[2] = 2;
            //Serial.print(jets[0]);
            //Serial.print(jets[1]);
            //Serial.print(jets[2]);
          
            while(jets[0] != 100 && jets[1] != 100 && jets[2] != 100){
            
              if(millis()-timing > 4000){
                timing = millis();
                //($2 3;) - конец модуля записи
                Serial.println("$2 3;");
              }
              parsingJets();
            }
            timing = 0;
            delay(300);
            parsing_stop();

            break;

          default:
            digitalWrite(indA, HIGH);
            digitalWrite(indB, HIGH);
          }
        }
        //после получения ($2 9 0;) - само включение М
        digitalWrite(indA, LOW);
        delay(2000);
        digitalWrite(indA, HIGH);

        digitalWrite(zum, LOW);
        delay(2000);
        digitalWrite(zum, HIGH);

        digitalWrite(zum, LOW);
        delay(500);
        digitalWrite(zum, HIGH);

        digitalWrite(zum, LOW);
        delay(500);
        digitalWrite(zum, HIGH);

        digitalWrite(indM, LOW);
        flagM = 1;

        jets[1] = 666;

        break;

      // калибровка
      case 4:
        int i = 0;

        while (jets[0] == 4 && jets[1] != 3) {
          //($4 3;) - команда об окончании калибровки

          bool stop_flag = 0;
          parsingJets();

          switch (jets[1]) {   
          case 1:
            //Serial.println("AAAAAAA");
            i = 0;
            if (jets[2] == 0) {
              jetMove_1(speedJet, speedStep);
            } else {
              while (!stop_flag) {
                if (speedJet > 0.0) {
                  jetMove_0(speedJet, speedStep);
                } else stop_flag = 1;
              }
              jetMove_angle_1(speedJet, speedStep, jets[2]); // цикл while
              jets[1] = 0;
              jets[2] = 0;
            i = 1;
            }
            break;

          case 2:
            i = 0;
            if (jets[2] == 0) {
              jetMove_2(speedJet, speedStep);
            } else {
              while (!stop_flag) {
                if (speedJet > 0.0) {
                  jetMove_0(speedJet, speedStep);
                } else stop_flag = 1;
              }
              jetMove_angle_2(speedJet, speedStep, jets[2]); // цикл while
              jets[1] = 0;
              jets[2] = 0;
              i = 1;
            }
            break;

          case 4:
            while(jets[0] != 100 && jets[1] != 100 && jets[2] != 100){
            
              if(millis()-timing > 4000){
                timing = millis();
                //подтверждение калибровки
                Serial.println("$4 2;");
              }
              parsingJets();
            }
            timing = 0;
            delay(300);
            parsing_stop();
            //Serial.println("$4 2;");
            jets[0] = 4;
            jets[1] = 0;
            jets[2] = 0;
            break;

          default:
            if(i == 1){
              jetMove_0angle(speedJet, speedStep);
            } else jetMove_0(speedJet, speedStep);
          }
        }

        jetMove_0(speedJet, speedStep);

        while(jets[0] != 100 && jets[1] != 100 && jets[2] != 100){
        
          if(millis()-timing > 4000){
            timing = millis();
            //подтверждение окончания калибровки
            Serial.println("$4 0;");
          }
          parsingJets();
        }
        timing = 0;
        delay(300);
        parsing_stop();
        //Serial.println("$4 0;");
        jets[0] = 0;
        jets[1] = 0;
        jets[2] = 0;
        break;
      //ФАРЫ
      case 5:
        //СВЕТ, если нужен при калибровке, команда от Джетс($5 1;)
        digitalWrite(indF1, LOW);
        digitalWrite(indF2, LOW);
        light_jets = 1;
        while(jets[0] != 100 && jets[1] != 100 && jets[2] != 100){
          if(millis()-timing > 4000){
            timing = millis();
            //подтверждение о включении ФАР
            Serial.println("$5 2;");
          }
          parsingJets();
        } 
        timing = 0;
        delay(300);
        parsing_stop();
        break;

      default:
        jetMove_0(speedJet, speedStep);
      }
    }

    //Serial.println("STOP - module 2");
    jets[0] = 3;
    jets[1] = 0;
    jets[2] = 0;

    enablePCINT(digitalPinToPCINT(BU));
    enablePCINT(digitalPinToPCINT(BD));
    enablePCINT(digitalPinToPCINT(BR));
    enablePCINT(digitalPinToPCINT(BL));
  }

  //flagM=1;
  //jets[0] = 3;
  ///////////////////////////
  if ((flagA_click || flagB_click) && flagM) {
    

    if (flagA_click) {
      while(jets[0] != 100 && jets[1] != 100 && jets[2] != 100){
        
        if(millis()-timing > 4000){
          timing = millis();
          // начать движение в точку A
          Serial.println("$3 1 0;");
        }
        parsingJets();
      }
    } else {
      while(jets[0] != 100 && jets[1] != 100 && jets[2] != 100){
        
        if(millis()-timing > 4000){
          timing = millis();
          // начать движение в точку Б
          Serial.println("$3 1 1;");
        }
        parsingJets();  
      }    

    } 
    timing = 0;
    delay(300);    
    parsing_stop();

    flagA_click = 0;
    flagB_click = 0;
    // Пред команда $3 0 0
    // условие выхода из цикла, команда от джетс $3 8 0 или юзер
    int i = 0;
    bool exit_user = 0;
    bool exit_jets = 0;
    
    //Включить УЗД - 1
   // Serial3.println("$1;");
    //Off_UZD();
    uzd_on();

    Prob_M_on();

    while (exit_jets == 0 && exit_user == 0) {
      //узд получаем

      //Serial.println("AAAA");
      bool stop_flag = 0;
      parsingJets();
      parsing_UZD();

      switch(jets[0]){

      case 3:
        switch (jets[1]) {
        case 1:
          //Serial.println("AAAA");
          i = 0;
          // половина скорости
          if(jets[2] == 0){
            jetMove_1(speedJet, speedStep);
          } else {
            while(!stop_flag){
              if(speedJet > 0.0){
                jetMove_0(speedJet, speedStep);
              } else stop_flag = 1;
            }
            jetMove_angle_1(speedJet, speedStep, jets[2]); // цикл while
            jets[1] = 0;
            jets[2] = 0;
            i = 1;
          }
          break;

        case 2:
          i = 0;
          // полная скорости
          if(jets[2] == 0){
          jetMove_2(speedJet, speedStep);
          } else {
            while(!stop_flag){
              if(speedJet > 0.0){
                jetMove_0(speedJet, speedStep);
              } else stop_flag = 1;
            }
            jetMove_angle_2(speedJet, speedStep, jets[2]); // цикл while
            jets[1] = 0;
            jets[2] = 0;
            i = 1;
          }
          break;

        case 99:
          // актуатор
          module3_stop(speedJet, speedStep);

          digitalWrite(akt_up, LOW);
          delay(5000); // ждем 12 секунд пока сработает актуатор
          digitalWrite(akt_up, HIGH);
          digitalWrite(akt_down, LOW);
          delay(5000);
          digitalWrite(akt_up, HIGH);
          jets[0] = 3;
          jets[1] = 0;
          jets[2] = 0;
          break;

        case 11:
          //отключить УЗД так как ручное управление
          uzd_off();
          // запрос ручного управления пока тумблер??
          module3_stop(speedJet, speedStep);
          // выключить М
          digitalWrite(indM, HIGH);
          flagM = 0;
          // орет клаксон
          digitalWrite(klax, LOW);

          if (!digitalRead(Ru_M)) {

            digitalWrite(klax, HIGH);
            // выключаем клаксон при переходе на ручной режим
            while (!flagM) {
              // и движемся
              butMov(speedH_2, speedH_3, speedStep);
              parsingJets(); // жду в цикле дижения команды включения М (3 9;)
              if (jets[0] == 3 && jets[1] == 9) flagM = 1;
            }
            // включить светодиод М
            digitalWrite(indM, LOW);
            jets[0] = 3;
            jets[1] = 0;
            // продолжаем двигаться по траектории
            while(jets[0] != 100 && jets[1] != 100 && jets[2] != 100){
              
              if(millis()-timing > 4000){
                timing = millis();
                Serial.println("$3 2;");
              }
              parsingJets();
            }
            //включить УЗД после окончания ручного управления
            uzd_on();
            timing = 0;
            delay(300);
            parsing_stop();
            //Serial.println("$3 2;");
            }     
            break;

        case 8: 
          module3_stop(speedJet, speedStep);

          exit_jets = 1;
          jets[0] = 0;
          jets[1] = 0;
          break;

        default:
          if(i == 1){
            jetMove_0angle(speedJet, speedStep);
          }else jetMove_0(speedJet, speedStep);
        }

      /*case 6:
       //УЗД
        if(jets[0] == 6 && jets[1] == 1){
          //parsing_UZD();
          Serial.print("$6 ");

          for(int i = 0; i < 6; i++){
            Serial.print(UZD[i]);
            Serial.print(' ');
          }
          Serial.print('\n');
          jets[0] = 0;
          jets[1] = 0;
        }
        break;  */
      case 5:
        //СВЕТ при движении по записанной траектории
        if(jets[0] == 5 && jets[1] == 1){
        digitalWrite(indF1, LOW);
        digitalWrite(indF2, LOW);
        light_jets=1;
        while(jets[0] != 100 && jets[1] != 100 && jets[2] != 100){
          if(millis()-timing > 4000){
            timing = millis();
            Serial.println("$5 2;");
          }
          parsingJets();
        }
        timing = 0;
        delay(300);
        parsing_stop();
        }

        break;

      default:
        jets[0] = 3;
        jets[1] = 0;
        /*if(i == 1){
          jetMove_0angle(speedJet, speedStep);
        } else jetMove_0(speedJet, speedStep);     
        jets[0] = 0;
        jets[1] = 0;
        break;*/
      }
      
      //Заготовка под УЗД
      /*if(millis() - timing > 10000){
        //проверка состояния УЗД каждые 10 сек
        timing = millis();
        uzd_to_jets(speedJet, speedStep);
      }*/

      if(!digitalRead(bStop)){
        jetMove_0(speedJet, speedStep);
        exit_user = 1;
        while(jets[0] != 100 && jets[1] != 100 && jets[2] != 100){
          
          if(millis()-timing > 4000){
            timing = millis();
            //остановка оператором через СТОП, отправка комады Джетс
            Serial.println("$3 0;");
          }
          parsingJets();
        }
        //Serial.println("$3 0;");
      }       
    } 
    //Окончательно отключаем УЗД после 3 модуля
    uzd_off();
    //offUzd = 0;

    timing = 0;
    delay(300);
    parsing_stop();
    //Serial.println("stop_module3");
    Prob_M_off();

  }

  // Актуатор и свет с пульта управления
  if (!digitalRead(butAct_up)) {
    digitalWrite(akt_up, LOW);
  } else digitalWrite(akt_up, HIGH);

  if (!digitalRead(butAct_down)) {
    digitalWrite(akt_down, LOW);
  } else digitalWrite(akt_down, HIGH);


  if(light_jets){
    if(!digitalRead(buttF1)){
      digitalWrite(indF1, HIGH);
      digitalWrite(indF2, HIGH);
      light_jets = 0;
    }
  }else{

    if(!digitalRead(buttF1)){
      digitalWrite(indF1, LOW);
    } else if(!digitalRead(buttF2)){
      digitalWrite(indF1, LOW);
      digitalWrite(indF2, LOW);      
    } else {

      digitalWrite(indF1, HIGH);
      digitalWrite(indF2, HIGH);      
    }

  }

  // Батарейка
  int batterySensorValue = 0;
  float batteryVoltage = 0;

  batterySensorValue = analogRead(A0);

  // коэффициент делителя
  const float dividerRatio = 0.167;

  batteryVoltage = batterySensorValue * 4.53 / 1023 / 0.167;

  //Serial.println(batteryVoltage);
  float bat_diff = batteryVoltage - 23;

  float batPercent = bat_diff * 100 / 1.5;

  //Serial.println(bat_diff);
  //Serial.println(batPercent);
  //delay(1000);
  
  if (batPercent <= 75) {
    digitalWrite(indBat100, HIGH);
  } else digitalWrite(indBat100, LOW);

  if (batPercent <= 50) {
    digitalWrite(indBat75, HIGH);
  } else digitalWrite(indBat75, LOW);

  if (batPercent <= 25) {
    digitalWrite(indBat50, HIGH);
  } else digitalWrite(indBat50, LOW);

  //программно отметить, что при ручном управлении и при движении с меткой 
  //отпралять Дане (нанке), чтобы УЗЛ не раегировало на препятсиве, 
  //не отключал питание от двигателей
}
