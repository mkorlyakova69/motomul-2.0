import logging

import cv2
import numpy as np
import math
from time import time

import config
# import LOG_FACE_QUALITY, LOG_FACE_QUALITY_KEY, FACE_QUALITY_LIMITS


class FrameQualityEstimator:
    r = 0
    g = 1
    b = 2

    def __init__(self):
        '''
        '''
        self.frame_init()

    def frame_init(self):
        self.quality_estimation = {}


        self.quality_estimation['size'] = [0, False]
        self.quality_estimation['shape'] = [0, False]
        self.quality_estimation['brightness'] = [0, False]
        self.quality_estimation['contrast'] = [0, False]
        self.quality_estimation['focus'] = [0, False]
        self.quality_estimation['illumination'] = [0, False]
        self.quality_estimation['sharpness'] = [0, False]

    def quality_estimation_go(self, image):
        '''

        :return:
        '''
        self.frame_init()
       # basic frame parameters from Khaechevnikova2021
        self.basic_frame_parameters(image)

        self.quality_criteria()

        return self.quality_estimation, self.quality_flag


    def basic_frame_parameters(self, image):
        '''
        basic frame parameters from Khaechevnikova2021

        :return:
        brightness

        '''
        self.quality_estimation = {}

        self.quality_estimation['size'] = [0, False]
        self.quality_estimation['shape'] = [0, False]
        self.quality_estimation['brightness'] = [0, False]
        self.quality_estimation['contrast'] = [0, False]
        self.quality_estimation['focus'] = [0, False]
        self.quality_estimation['illumination'] = [0, False]
        self.quality_estimation['sharpness'] = [0, False]

        h,w,c = image.shape
        self.quality_estimation['size'] = [h*w, False]
        self.quality_estimation['shape'] = [len(image.shape), False]
        intensity = ((0.2126 * image[..., self.r]) + (0.7152 * image[..., self.g]) + (0.0722 * image[..., self.b])) / 255.
        avg_standard_lum = np.sum(intensity) / (w * h)
        avg_contrast =  math.sqrt((np.sum(intensity ** 2) / (h * w)) - (avg_standard_lum ** 2))
        focus =self.blurry(image)

        self.quality_estimation['brightness'] = [avg_standard_lum, False]
        self.quality_estimation['contrast'] = [avg_contrast, False]
        self.quality_estimation['focus'] = [focus, False]
        self.quality_estimation['sharpness'] = [self.blurry(image), False]#### TO DO:: not sharpness
        self.quality_estimation['illumination'] = [self.illumination(image[:,:,0]), False]

    def blurry(self, image):
            return cv2.Laplacian(image, cv2.CV_64F).var()


    def illumination(self , gray):
        # length of R available  range  of  gray  intensities  excluding  5%  of  the darkest  and  brightest  pixel
        sorted_gray = np.sort(gray.ravel())
        len_img = len(sorted_gray)
        cut_off_idx = len_img * 5 // 100
        r = sorted_gray[len_img - cut_off_idx] - sorted_gray[cut_off_idx]
        return np.round(r / 255, 2)
    ##################
    def quality_criteria(self):
        '''
        location: ellipse par:
        Screen:
            self.center_coordinates = (self.frame_width // 2, self.frame_height // 2)
            self.axesLength = (self.frame_height // 3, self.frame_width // 4)
            self.angle, self.startAngle, self.endAngle = 0, 0, 360

        :return:
        '''

        limits = config.FACE_QUALITY_LIMITS

        for key, value in limits.items():
            self.quality_flag = True
            if len(self.quality_estimation[key]) != 2:
                self.quality_estimation[key][0] = 0
                self.quality_estimation[key][1] = True
            else:
                if (self.quality_estimation[key][0] >= value[0]) and (self.quality_estimation[key][0] <= value[1]):
                    self.quality_estimation[key][1] = True
                else:
                    # if LOG_FACE_QUALITY:
                    # logging.info('poor quality: %s = %s' % (key, self.quality_estimation[key][0]))

                    self.quality_flag = False

    def quality_log(self):
        for key, item in self.quality_estimation.items():
            if not item[1]:
                logging.info('poor quality: %s: %s' % (key, item[0]))
            else:
                logging.info('good quality: %s: %s' % (key, item[0]))

