import math
import time
import numpy as np
import matplotlib.pyplot as plt


T1 = time.time()
v = 1

const_v = 5 # скорость мотомула в нормальном режиме
Norma = 5 #расстояние для нормальной скорости

DEGREE = 0
Target_traect = []
Motomul_traect = [[0., 0.]]

def  traectory_move(angle, distance, v , logger = None) :
    ### moving with target

    # DEGREE -- sum (all commands to turn)
    # Coord -- x,y, motomul from real 0,0
    # Motomul_traect -- all old Coord
    # Target_traect -- all ald points of target (Coord_target)
    global Motomul_traect
    global Target_traect
    global DEGREE
    global T1
    if  logger is not None:
        logger.info('kate_into: '+str(angle)+'_'+str(distance)+'_'+str(v))
    DEGREE_r = DEGREE / 180 * np.pi
    ### sending -- from cam: angle, distance
    sending = [angle, distance]
    if  logger is not None:
        logger.info('kate_into: '+str(sending[0])+'_'+str(sending[0]))
    Coord_target = [0., 0., 1.]
    Coord = Motomul_traect[-1]
    Coord_n=Coord.copy()
    if sending:
        Coord_target[0] = distance * np.cos(angle / 180 * np.pi)
        Coord_target[1] = distance * np.sin(angle / 180 * np.pi)
        Coord_target[2] = 1.

        Matrix_Transform = np.array([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [-Coord[0], -Coord[1], 1.0]])
        Matrix_Rotate = np.array(
            [[np.cos(-DEGREE_r), np.sin(-DEGREE_r), 0.0], [-np.sin(-DEGREE_r), np.cos(-DEGREE_r), 0.0], [0.0, 0.0, 1.0]])

        Coord_target = np.dot(Coord_target, Matrix_Rotate)
        Coord_target = np.dot(Coord_target, Matrix_Transform)


       # Coord_target[0] = Coord[0] + distance * np.cos(angle/180*np.pi + DEGREE_r)
       # Coord_target[1] = Coord[1] + distance * np.sin(angle/180*np.pi + DEGREE_r)
        #write target coordinats to Target_traect
        Target_traect += [Coord_target]


    way = (time.time()-T1) * v * const_v
    T1=time.time()
    if  logger is not None:
        logger.info('kate_into: way,T1'+str(way)+'_'+str(T1))

    Coord_n[0] = Coord[0] + way * np.cos(DEGREE_r)
    Coord_n[1] = Coord[1] + way * np.sin(DEGREE_r)

    Motomul_traect += [ Coord_n.copy()]
    if  logger is not None:
        logger.info('kate_into: Mot_tra'+str(Coord_n[0])+'_'+str(Coord_n[1]))

    #отсечь точки позади
    #######skimage.transform.matrix_transform(traectory)
    ### find next step as sqrt( (xt-xm)^2 + (yt-ym)^2 )
    traectory = np.array(Target_traect)

    Matrix_Transform  = np.array([[1.0,0.0,0.0],[0.0, 1.0, 0.0],[Coord[0],Coord[1],1.0]])
    Matrix_Rotate= np.array([[np.cos(DEGREE_r) , np.sin(DEGREE_r), 0.0] , [-np.sin(DEGREE_r) , np.cos(DEGREE_r), 0.0] , [0.0,0.0,1.0]])

    traectory2 = np.dot(traectory, Matrix_Transform)
    traectory2 = np.dot(traectory2, Matrix_Rotate)
    if  logger is not None:
        logger.info('kate_into: tra'+str( traectory2[0]))

    d = np.where(traectory2[:,0]>0)[0]
    try:
	
        traectory = traectory[d,:]
    except:
    	traectory = np.array([])
    
    if  (logger is not None) and (len(traectory)>0):
        logger.info('kate_into: d,tra'+str(d[0])+'_'+str(traectory[0])+'_'+str(len(traectory)))
    else:
        logger.info('kate_into: d,tra empty '+'_!')
    if len(traectory):
        d =np.linalg.norm(traectory[:,:-1]-np.array(Coord), axis=1)
        Nearest = traectory[np.argmin(d)][:] # x and y of our next step
        Length = d[np.argmin(d)] # vector of distanse between motomul and target traectory
        if  (logger is not None):
            logger.info('kate_into: '+'Length:'+str(Length))
    else:
        traectory = np.array(Target_traect)
        Nearest = traectory[-1,:-1]  # x and y of our next step
        Length = np.linalg.norm(traectory[-1,:-1] - np.array(Coord))  # vector of distanse between motomul and target traectory
        if  (logger is not None):
            logger.info('kate_into: '+'Length:'+str(Length))
    print(Nearest)
    ###
    if (Nearest[1]-Coord[1]) == 0:
        DEGREE_curr = np.sign(Nearest[0] - Coord[0]) * 90
        if  (logger is not None):
            logger.info('kate_into: '+'DEGREE_curr1:'+str(DEGREE_curr))
    else:
        DEGREE_curr = np.sign(Nearest[0]-Coord[0])* int(np.arctan( (Nearest[1]-Coord[1])/(Nearest[0]-Coord[0]) )* 180/np.pi - DEGREE) // 10 * 10
        if  (logger is not None):
            logger.info('kate_into: '+'DEGREE_curr2:'+str(DEGREE_curr))


    ##### округлить до десятков!!!!!

    if Length > Norma:
        v = 2
    else:
        v = 1

    if np.abs(DEGREE_curr) > 90:
        v = 0
        print(DEGREE_curr)
        DEGREE_curr = 0

    DEGREE = DEGREE + DEGREE_curr
    if  logger is not None:
        logger.info('kate_into: return'+str( DEGREE)+'_'+str(DEGREE_curr)+'_'+str(v))
    #print(DEGREE_curr)
    print("angle:",DEGREE_curr,'  : v:',v)
    #send(DEGREE,v) #command message for arduino
    return DEGREE_curr,v


def traectory_simple(angle, distance, v ):
        ###Заглушка (по флагу при вызове функции, например)

        v = 1
        DEGREE_curr = int(angle) // 10 * 10
        # ЕХАТЬ В СТОРОНУ ТОЛЬКО ЧТО НАЙДЕННОЙ ЦЕЛИ, не рассчитывая траекторию
        if (distance<1000):
            v = 0
        if (distance > 1500):
            v = 2
        return DEGREE_curr, v

#
# plt.figure(figsize = (16,5))
# for i in range(10):
#     d,v = traectory_move(12, 100, v )
#     time.sleep(1)
#     plt.plot(Target_traect[-1][0],Target_traect[-1][1],'bo')
#     plt.plot(Motomul_traect[-1][0], Motomul_traect[-1][1], 'ko')
#     print('Moto:',Motomul_traect[-1],'  traect:',Target_traect[-1] )
# plt.show()
#


