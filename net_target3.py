import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import tensorflow as tf
from tensorflow.python.saved_model import tag_constants

import cv2
from cv2 import aruco
from scipy import ndimage

import os
import logging
import time

import model
import config

assert tf.__version__.startswith('2')
gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

# create tracker
(major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
if int(minor_ver) < 3:
        tracker = cv2.Tracker_create(tracker_type)
else:
        tracker = cv2.TrackerKCF_create()

logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')

file_handler = logging.FileHandler('logs_cam.log')
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)
# logger.info('log message')
# logger.error('error message.')

#aruco constant
ARUCO_DICT = aruco.Dictionary_get(aruco.DICT_4X4_250)
SIZE_ARUCO = 0.044  # side length in meters
ARUCO_COEF =  200./936.*1800./175.*550

AROCU_PAR = aruco.DetectorParameters_create()
# camera calibrate constant
# camera matrix
MCC = np.array([[897.88895934 ,  0.  ,       322.35676237],
 [  0.    ,     895.69226588 ,262.84802736],
 [  0.   ,        0.  ,         1.  ,      ]])
# distortion matrix
DM = np.array([[ 0.108941582, -0.672533902,  0.0334419961, -0.00716088448,
   10.8825779]])
# use jetson (1) 
JETSON = 0
if JETSON == 0:
    save_dir = 'model/new_tflite'
    #save_dir = 'model/tflite_24_04'
    #model.convert(modelu,save_dir )
    model_name = 'model.tflite'#'mod_FLOAT32.tflite'#convert(modelu,save_dir )

else:
    #save_dir = 'model/new_tflite'
    #save_dir = 'model/tflite_24_04'
    save_dir = 'model/new_tflite_06'
    # model.convert(modelu,save_dir )


count = 0
# model config constant (don't changed without check model type)_
list_n = 1
BN = 1
nf = 2
SCALE = 1.0
# get model variant : 0 - saved weights model, 1 - tflite model, 2 - saved model .pb
tf_l = 0
# get model from saved model
if tf_l == 0:

    my_signature = None
    # my_signature.allocate_tensors()
    # Get input and output tensors.
    input_details = [0, 0]
    output_details = [0, 0]

    if 1:  # config.TRAIN:
        # create model
        modelu,_,_ = model.LiNet_pre_train_seg_3(num_classes=14, input_shape=(128, 256, 3), pre_trained_model=None, modelt_weight='model/new_128_256_25_04.hdf5',
                                           pretrain_out=1,
                                           list_bloc=[10] * list_n, BN=BN, nf=nf)
        # modelu = model.LiNet_pre_train_seg(num_classes=2, input_shape=(512, 1024, 3), pre_trained_model=None, pretrain_out=1,
        #                                  list_bloc=[10]*list_n, BN=BN, nf=nf)
        modelu.summary()
        try:
            # load weights
            modelu.load_weights('model/new_128_256_new_seg_target_depth.hdf5')  # net_target0'+str(list_n)+'_'+str(nf)+'.hdf5')
            print("checkpoint load ****************")
        except:
            logger.info('no weight for model')
    else:
        # saved model
        modelu = tf.keras.models.load_model('model/new_tflite')  # /saved_model.pb')

rez = []
n = 0
# tflite model
if tf_l == 1:
    my_signature = model.inference()
    my_signature.allocate_tensors()
    # Get input and output tensors.
    input_details = my_signature.get_input_details()
    output_details = my_signature.get_output_details()
    my_signature.invoke()
    print('read tflite:', my_signature)

if tf_l == 2:
    print('TR************')
    saved_model_loaded = tf.saved_model.load('model/tenorRT', tags=[tag_constants.SERVING])
    signature_keys = list(saved_model_loaded.signatures.keys())
    print(signature_keys)
    my_signature = saved_model_loaded.signatures['serving_default']
    print("tr model load *****************")
    input_details = None
    output_details = None


def segment_get(yp_segment: object, y_depth: object, plot_im: int = 0) -> list:
    class_name = ['o1', 'o2', 'o3', 'o4', 'o5', 'o6', 'o7', 'o8', 'o9', 'o10', 'o11', 'o12', 'o13', 'o14']
    y_work = yp_segment.numpy() * y_depth.numpy()
    print('+++++++++++++++++++++++++++segment ok')
    list_rez = []
    for i, name in enumerate(class_name):
        y_max, y_min = y_work[0,:,:,i].max(), y_work[0,:,:,i].min()
        list_rez +=  [[i, name, y_max, y_min]]
        #if (y_max != y_min) and (y_max != 0) and (y_min != 0):
        #    list_rez +=  [[i, name, y_max, y_min]]

    return  list_rez

def target_get(image, image_origin_shape = (128, 256), low=128, hi=255, plot_im = 1):
    """
    выбираем области замыкания в изображении. если их нет, то считаем, что нет интересных объектов
    
    :param image: numpy array - картинка (массив нампи)
    :param image_origin_shape: list[int, int] image original size(input from cam)
    :param low: int - уровень отсечки фильтра снизу для бинаризации
    :param hi: int - уровень отсечки фильтра сверху для бинаризации
    :param plot_im: int - 1, - рисовать результаты 1. нет 0
    :return: list[] list_im - list of result image
    """

    im_scale = 2
    image = cv2.resize(image, (image.shape[1]//im_scale, image.shape[0]//im_scale,))
    th, im_th = cv2.threshold(image, low, hi, cv2.THRESH_BINARY_INV)

    # Copy the thresholded image.
    im_floodfill = im_th.copy()

    # Mask used to flood filling.
    # Notice the size needs to be 2 pixels than the image.
    h, w = im_th.shape[:2]
    mask = np.zeros((h + 2, w + 2), np.uint8)

    # Floodfill from point (0, 0)
    cv2.floodFill(im_floodfill, mask, (0, 0), 255)

    # Invert floodfilled image
    im_floodfill_inv = im_floodfill

    # Combine the two images to get the foreground.
    im_out = im_th | im_floodfill_inv

    m = 3
    kernel = np.ones((5, 5), np.uint8)

    img_erosion = cv2.erode(im_out, kernel, iterations=m)
    img_dilation = cv2.dilate(img_erosion, kernel, iterations=m)
    img_dilation = cv2.resize(img_dilation, (image_origin_shape[1], image_origin_shape[0]))

    # find connected components
    if plot_im:
        plt.subplot(1,2,1)
        plt.imshow(img_dilation)
        plt.subplot(1, 2, 2)
        plt.imshow(image)
        plt.show()

    contours, hierarchy = cv2.findContours(255-img_dilation, cv2.RETR_LIST , cv2.CHAIN_APPROX_SIMPLE)

    if len(contours) >0:
        d = []
        contours_win = []
        for c in contours:
            cn = np.array(c)
            d += [(cn[:,0,0].max()-cn[:,0,0].min())*(cn[:,0,1].max()-cn[:,0,1].min())]
            contours_win += [cn]

        return img_dilation,contours_win
    else:
        return img_dilation,[]

def image_prep(img, im_size = (512,1024)):
    """
    
    :param img: - input image (numpy or cv2)
    :param im_size: - target size
    :return: tensor 4D
    """
    img = tf.image.convert_image_dtype(img, dtype=tf.float32)
    img_resized = tf.image.resize(img, im_size, method='bicubic', preserve_aspect_ratio=False)
    return tf.reshape(img_resized,[1,im_size[0],im_size[1],3])


# noinspection PyPep8Naming
def findCenter(p1, p2):
    """
    get centroid
    :param p1: point1 vector
    :param p2: point2 vector
    :return: centroid vector
    """
    center = ((p1[0] + p2[0]) // 2, (p1[1] + p2[1]) // 2)
    return center

def find_center_aruco(xy = None):
    """
    get center target image - from aruco indicated in current frame
    :param xy: vectors from aruco coordinate point
    :return: target center
    """
    l_point = []
    if xy is not None and len(xy) > 0:
        # find 3 aruco marker
        if xy.shape[0] == 3:
            ind = np.argsort(xy[:, 3])
            # l_point = xy[:,:3].tolist()
            l_point = [np.mean(
                np.array(xy[ind[0], 4])[[0, 3], :].tolist() + np.array(xy[ind[1], 4])[[1, 2], :].tolist() +
                np.array(xy[ind[2], 4])[[0, 3], :].tolist(), axis=0).tolist() + [xy[0, 2]]] * 3
        # find 2 aruco marker
        elif xy.shape[0] == 2:
            ind = np.argsort(xy[:, 3])
            if xy[ind[0], 3] == 0 and xy[ind[1], 3] == 2:
                c1 = np.mean(np.array(xy[ind[0], 4])[[0, 3], :].tolist() +
                             np.array(xy[ind[1], 4])[[0, 3], :].tolist(), axis=0).tolist() + [xy[0, 2]]

            else:
                if xy[ind[0], 3] == 0:
                    c1 = np.mean(np.array(xy[ind[1], 4])[[1, 2], :], axis=0).tolist() + [xy[0, 2]]
                else:
                    c1 = np.mean(np.array(xy[ind[0], 4])[[1, 2], :], axis=0).tolist() + [xy[0, 2]]

            l_point = [c1, c1, c1]
        # find 1 aruco marker
        elif xy.shape[0] == 1:
            if xy[0, 3] == 0:
                c1 = (np.array(xy[0, 4])[3, :] + (np.array(xy[0, 4])[3, :] - np.array(xy[0, 4])[0, :])).tolist() + [
                    xy[0, 2]]
            elif xy[0, 3] == 1:
                c1 = np.mean(np.array(xy[0, 4])[[1, 2], :], axis=0).tolist() + [xy[0, 2]]
            else:
                c1 = (np.array(xy[0, 4])[0, :] + (np.array(xy[0, 4])[0, :] - np.array(xy[0, 4])[3, :])).tolist() + [
                    xy[0, 2]]
            l_point = [c1, c1, c1]
        # no find  aruco marker
        elif xy.shape[0] == 0:
            l_point = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        else:# find >3 aruco marker
            l_point = xy[:3, :3].tolist()

    else:
        l_point = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    return l_point

def test_target(im, x, y, w, h, im_size, plot_im = 1, im_big = None):
    """
    take angle and distance for target in RoI
    :param im: - RoI from im_big
    :param x: int - x coordinate left-top corner of RoI
    :param y: int - y coordinate left-top corner of RoI
    :param w: int  - width of RoI
    :param h: int  - height of RoI
    :param im_size: list[int, int] - im_big size
    :param plot_im: int - plot(1) or not plot(0) result
    :param im_big: numpy array - image > im
    :return: tuple[float, float, float] - detect, angle, distance
    """
    s = ['or', 'ob', 'og', '+r', '+g', '+b', '*r', '*g', '*b']
    if im is not None:
        Angle, Distance = 0.0, 0.0
        logger.info('image is not None:' + str(im.shape[0]) + '_' + str(im.shape[1]))

        im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        im_ = (im - im.min()) * 255.0 / (im.max() - im.min())
        im_ = im_.astype(np.uint8)
        markerCorners, markerIds, rejectedCandidates = cv2.aruco.detectMarkers(im_, ARUCO_DICT, parameters=AROCU_PAR)
        if plot_im:
            if im_big is not None:
                plt.subplot(1,2,2)
                plt.imshow(im_big[0,:,:,:])
            
            im_ = cv2.aruco.drawDetectedMarkers(image=im_, corners=markerCorners, ids=markerIds, borderColor=(255, 255, 255))

        # Draw rejected markers:
            im_ = cv2.aruco.drawDetectedMarkers(image=im_, corners=rejectedCandidates, borderColor=(0, 0, 255))
            plt.subplot(1, 2, 1)
            plt.imshow(im_)
            plt.show()
        xy = []
        if markerIds is not None:
            markerCorners_buf, markerIds_buf = [],[]
            for m, corner in zip(markerIds,markerCorners):
                if m[0] in [1,2,3]:
                    corner[0, :, 1] = corner[0, :, 1] + y
                    corner[0, :, 0] = corner[0, :, 0] + x
                    markerCorners_buf += [corner]
                    markerIds_buf += [m]
            markerCorners, markerIds = markerCorners_buf, markerIds_buf
        logger.info('image is not None: markerIds:' + ','.join( [str(id_marck) for id_marck in markerIds] ))
        if markerIds is not None:
            for corner, m in zip(markerCorners, markerIds):
                dx1 = np.sum((corner[0, 0, :] - corner[0, 1, :])**2 ) ** 0.5
                dy1 = np.sum((corner[0, 2, :] - corner[0, 1, :]) ** 2) ** 0.5
                dx2 = np.sum((corner[0, 3, :] - corner[0, 2, :]) ** 2) ** 0.5
                dy2 = np.sum((corner[0, 3, :] - corner[0, 0, :]) ** 2) ** 0.5
                d1 = (dx1 ** 2 + dy1 ** 2) ** 0.5
                d2 = (dx2 ** 2 + dy2 ** 2) ** 0.5
                d = d1 if d1>d2 else d2
                rvec, tvec, markerPoints = aruco.estimatePoseSingleMarkers(corner,SIZE_ARUCO,MCC,DM)# pose vector (for new version target)
                xy += [[np.mean(corner[0, :, 1]), np.mean(corner[0, :, 0]), d , m[0]-1, corner[0, :, :]]]
            if plot_im:
                plt.imshow(im_)
                plt.show()
            xy = np.array(xy)
            
        if markerIds is not None:        

            l_point = find_center_aruco(xy)
            print(l_point)
            
            if xy.shape[0]>0:
                pts_src = np.array(l_point)
                dXY = np.array([[xy[:,0].max() - xy[:,0].min(), xy[:,1].max() - xy[:,1].min() ]])
                dy,dx = xy[:,2].max(),xy[:,2].min()
            else:
                pts_src = np.zeros((4,3))
                dy,dx = 1,1
            try:
                pts_src[:,0] = pts_src[:,0]#+x
                pts_src[:, 1] = pts_src[:, 1]# + y

                x_mean = np.mean(pts_src[:,0])
                # get target pose from frame center
                x_abs = (x_mean - (im_size[1] // 2 ))
                x_abs_ = x_abs#
                if dy <= dx:
                    z_abs_ = 1000 * SIZE_ARUCO / dx * ARUCO_COEF
                else:
                    z_abs_ = 1000 * SIZE_ARUCO/dy * ARUCO_COEF
                #result distance and angle for target
                Distance = (z_abs_ ** 2 + (x_abs_ * z_abs_ / 1000) ** 2) ** 0.5 * SCALE
                Angle = np.arcsin(x_abs_ / 1000) / np.pi * 180
                print("dist:", Distance, " : ang:",Angle)
            except:
                logger.error('image is not None: markerIds:')
                print('error get target distance-angle')
        else :
            # no target
            logger.info('image is not None: no markerIds:')
            Angle, Distance = 0., 0.
            
        if plot_im:
            plt.figure(figsize = (16, 6))
            plt.subplot(1,3,1)
            plt.imshow( im )
            if markerIds is not None:
                plt.plot(xy[:,1], xy[:,0], 'o')
                print('x,y :', x, y, pts_src)
            plt.subplot(1,3,2)
            if im_big is not None:
                im_Z = im_big[0,:,:,0]
            else:
                im_Z = np.zeros((500,700))

            im_Z[y:im_.shape[0]+y, x:im_.shape[1]+x] = im_
            plt.imshow( im_Z )
            if markerIds is not None:
                plt.plot(pts_src[:,0], pts_src[:,1], 'o')
            plt.show()
        logger.info('image is not None find target: distance, angle:' + str(Distance) + ',' + str(Angle) )
        return 1.0, Angle, Distance
    else:
        # no image
        logger.info('image is  None:')
        return 0.0, 0.0, 0.0

# поиск областей с мишенями
def coord_get(target_, im,  plot_im = 1):
    """
    get target from candidate RoI
    :param target_: list - contur RoI from im
    :param im: numpy array - input image (from cam)
    :param plot_im: int - 0 - not plot, 1 - plot
    :return: target_rez: list[] - list of founded target
    """
    #X_Y : min x, min y, max x, max y,  mean x, mean y, w, h

    X_Y = [[c[:, 0, 0].min(), c[:, 0, 1].min(), c[:, 0, 0].max(), c[:, 0, 1].max(),
            (c[:, 0, 0].max() + c[:, 0, 0].min()) / 2, (c[:, 0, 1].max() + c[:, 0, 1].min()) / 2,
            (c[:, 0, 0].max() - c[:, 0, 0].min()) * (c[:, 0, 1].max() - c[:, 0, 1].min())] for c in target_[1]]
    target_rez = []

    for k, c in enumerate(X_Y):
        if (c[-1] > 5) and (c[-1] < 100000):
            if im[0,c[1]-10:c[3]+10,c[0]-10:c[2]+10,:] is not None:
                cw = (c[2] -c[0])
                ch = (c[3]-c[1])
                if (im.shape[1]>0 and im.shape[2]>0)and(abs(cw-ch)/np.max([cw,ch]) < 0.7):
                    if (c[1]>10)and(c[0]>10):
                        p = test_target(im[0,c[1]-10:c[3]+10,c[0]-10:c[2]+10,:],c[0]-10,c[1]-10,c[2]+10-(c[0]-10),c[3]+10-(c[1]-10),im.shape[1:], plot_im =  plot_im, im_big=im)
                        #p = test_target(im[0,:,:,:], 0, 0,
                        #            im.shape[2], im.shape[1], im.shape[1:], plot_im=plot_im)
                        print(':p::::::::-----',p)
                        logger.info('coord end i1: '  + str(k) + ':' + str(p)  )

                    else:
                        p = test_target(im[0, :, :, :], 0, 0,
                                        im.shape[2], im.shape[1], im.shape[1:], plot_im=plot_im, im_big=im)
                        #p = test_target(im[0, c[1] :c[3] + 10, c[0]:c[2] + 10, :], c[0], c[1], c[2] + 10 - c[0] , c[3] + 10 - c[1], im.shape[1:], plot_im =  plot_im)
                        print(':p: -----', p)
                        logger.info('coord end i2: '  + str(k) + ':' + str(p)  )
                    try:
                        logger.info('coord end i: '  + str(k) + ':' + str(p)  )
                        target_rez += [[p[0], target_[1][k], c, p[1],p[2]]]
                    except:
                        logger.error('coord end i3: '  )

    logger.info('coord end : ' + str(len(target_rez))  )
    return target_rez

def net_target_get(image, my_signature = my_signature, input_details = input_details, output_details = output_details, plot_im = 0, tf_l = tf_l):#my_signature)   :
    # my_signature is callable with input as arguments.
    t1 =time.time()
    im = image.copy()
    if tf.shape(im).numpy()[0] == 0:
        im = np.random.randint(0, 255, (1, 128, 256, 3))
    im = image_prep(im, im_size=[128, 256])
    logger.info('net input 2 image prep:' + str(time.time() - t1)+' :'+str(tf_l) + ': ' + str(tf.shape(im) ))
    ## Get input and output tensors.
    #input_details = my_signature.get_input_details()
    # Test the model on random input data.
    if  tf_l == 1:
        input_shape = input_details[0]['shape']
        input_data = np.array(im, dtype=np.float32)
        my_signature.set_tensor(input_details[0]['index'], input_data)
        my_signature.invoke()
        # The function `get_tensor()` returns a copy of the tensor data.
        # Use `tensor()` in order to get a pointer to the tensor.
        print('******************************************************')
        yp_t_tflite  = my_signature.get_tensor(output_details[0]['index'])
        yp_t = yp_t_tflite
    elif tf_l == 2:
        yp_t = my_signature(tf.constant(tf.reshape(im,[-1,128,256,3])))
      
        yp_t = yp_t['conv2d_7'].numpy()
    else:
        print('new frame in net:*******************************')
        yp_t,yp_segment, y_depth = modelu(im)
        yp_t = yp_t.numpy()
        print('end new frame in net:*******************************',yp_t.shape)
    
    logger.info('net output 2 tflite:' + str(time.time() - t1))
     	
    try:
        t1 = time.time()
        logger.info('target 3:' + str(time.time() - t1) + ":" + '_'.join([str(k) for k in yp_t.shape]))
        target = yp_t[0, :, :, 0] #/ yp_t[0, :, :, 0].max()#yp_t.numpy()[0, :, :, 0] / yp_t.numpy()[0, :, :, 0].max()
        logger.info('target 31:' + str(time.time() - t1) + ":" + '_'.join([str(k) for k in target.shape]))
        t1 = time.time()
        target_ = target_get((target * 255).astype('uint8'),image.shape[:2] , low=50, hi=255, plot_im = plot_im)
        logger.info('target 33 end:' + str(time.time() - t1) + " : "  )
        segment_ = segment_get(yp_segment, y_depth, plot_im=plot_im)
        logger.info('target 31 end:' + str(time.time() - t1) + " : " + str(len(segment_))  )
        t1 = time.time()

        target_rez = coord_get(target_, image.reshape([-1]+list(image.shape)),  plot_im =  plot_im)#im.numpy()
        logger.info('coord target$' + ': ' + str(len(target_rez)))
        try:
            logger.info('message target$' + ':'.join([str(target_rez[0][i]) for i in [0,-2,-1]]))
        except:
            logger.info('message target$' + ': EMPTY')
        logger.info('target 32 end:' + str(time.time() - t1) )
        t1 = time.time()
        #print(target_rez)
        if plot_im:
            plt.subplot(1,3,1)
            plt.imshow(im.numpy()[0,:,:,:])
            try:
                plt.subplot(1, 3, 2)
                plt.imshow(target)
                xy = target_rez[0][2]
                plt.plot([xy[0],xy[2]],[xy[1],xy[3]],'r*')
                
                plt.subplot(1, 3, 3)
                plt.imshow(image[:, :, :])
                xy = target_rez[0][2]
                plt.plot([xy[0], xy[2]], [xy[1], xy[3]], 'r*')
            except:
                pass
            plt.show()
    except:
        logger.error('no work frame/target ' )
        target_rez, target_, segment_ = [], [np.array([[[]]]),[]], []
        
    return target_rez, target_, segment_
#net_target_get([])
