import cv2
#from pynput import keyboard
import config

import numpy as np
import time
from multiprocessing import shared_memory
import control
import arduino_test
import serial
import os
import shutil
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')

file_handler = logging.FileHandler('logs.log')
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

ARDUINO_work = config.ARDUINO_WORK

# logger.info('log message')
# logger.error('error message.')
time.sleep(30)
if 1:
    dict_conf = config.confir_read()
    # parse arguments
    temp_dir = dict_conf["temp_dir"]  #
    time_calib_cam = dict_conf["time_calib_cam"]
    time_calib_gyro = dict_conf["time_calib_gyro"]
    time_interval_detect = dict_conf["time_interval_detect"]#
    image_size_detect = dict_conf["image_size_detect"]  #
    name_detect_net = dict_conf["name_detect_net"]

    time_interval_gyro =dict_conf["time_interval_gyro"]

    N_BINS = dict_conf["N_BINS"]
    N_CAM = dict_conf["N_CAM"]
    print(N_BINS,N_CAM,'*******************')
    time.sleep(31)
    #C_CAM = np.zeros((8,N_CAM))
    #C_BINS = np.zeros((15,N_BINS))
    C_CAM = np.zeros((N_CAM, 8))
    C_BINS = np.zeros((N_BINS, 15))
    try:
        BINS_shm = shared_memory.SharedMemory(name='pipeline_bins')
        #time_bins, x, y, z, vx, vy, vz, ax, ay, az, lx, ly, lz, calibr
        BINS = np.array([[time.time(), 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,0.,0.,0.,0.,0.]]*N_BINS)
        logger.info('C_BINS shape:'+ str(BINS.shape[0])+'_'+ str(BINS.shape[1]))
        C_BINS = np.ndarray(BINS.shape, dtype=BINS.dtype, buffer=BINS_shm.buf)
        bins_shared = True
        logger.info('C_BINS run!')
    except:
        bins_shared = False
        logger.info('C_BINS not run!')
        logger.error('no main_pipeline_bins shared memory')
    try:
        CAM_shm = shared_memory.SharedMemory(name='pipeline_cam')
        ## time, frame Quality, Detector,Angle,Distance,problem, calibr, rezerv]

        CAM = np.array([[time.time(), 0.,0., 0.,  0., 1., 0., 0.]] * N_CAM)
        logger.info('CAM shape:'+ str(CAM.shape[0])+'_'+ str(CAM.shape[1]))
        C_CAM = np.ndarray(CAM.shape, dtype=CAM.dtype, buffer=CAM_shm.buf)

        cam_shared = True
        logger.info('C_CAM run!')
    except:
        cam_shared = False
        logger.info('C_CAM not run!')
        logger.error('no main_pipeline_cam shared memory')


    ARDUINO_DIR = dict_conf["ARDUINO_DIR"]
    ARDUINO_TIMEOUT = dict_conf["ARDUINO_TIMEOUT"]
    ARDUINO_F = dict_conf["ARDUINO_F"]
    ARDUINO_DIR = dict_conf["ARDUINO_DIR"]
    ARDUINO_TIMEOUT = dict_conf["ARDUINO_TIMEOUT"]

    ARDUINO_ROTATE = dict_conf["ARDUINO_ROTATE"]
    ARDUINO_WAIT = dict_conf["ARDUINO_WAIT"]
    ARDUINO_START = dict_conf["ARDUINO_START"]
    # sleep calibrate
    T30 = ARDUINO_START  # timeout to begin all modules
    T01 = ARDUINO_TIMEOUT  # timeout arduino
    T3 = ARDUINO_WAIT * 5  # timeout after calibrate
    T5 = ARDUINO_WAIT  # timeout to write point to dir
    T4 = ARDUINO_ROTATE  # timeout to rotate to 10 degree
    # gyro calib data
    a_x_right = dict_conf["a_x_r"]
    a_x_left = dict_conf["a_x_l"]
    a_x_0 = dict_conf["a_x_0"]

    r_far = dict_conf["r_far"]
    r_near = dict_conf["r_near"]
    r_0 = dict_conf["r_0"]

    # image directori to write
    DIR_CAM = dict_conf["DIR_CAM"]
    DIR_AB = dict_conf["DIR_AB"]
    DIR_BA = dict_conf["DIR_BA"]
    DIR_A = dict_conf["DIR_A"]
    DIR_B = dict_conf["DIR_B"]
    DIR_A1 = dict_conf["DIR_A1"]
    DIR_CALIB = dict_conf["DIR_CALIB"]

T_ANGLE =  0

if config.JETSON:
  if ARDUINO_work :
    try:
        arduino = serial.Serial(ARDUINO_DIR, ARDUINO_F, timeout=ARDUINO_TIMEOUT)
        arduino_run = True
        logger.info('arduino run!')
    except:
        try:
            arduino = serial.Serial('/dev/ttyUSB0', ARDUINO_F, timeout=ARDUINO_TIMEOUT)
            arduino_run = True
            logger.info('arduino run!')
        except:
            try:
                arduino = serial.Serial('/dev/ttyUSB1', ARDUINO_F, timeout=ARDUINO_TIMEOUT)
                arduino_run = True
                logger.info('arduino run!')
            except:
                try:
                    arduino = serial.Serial('/dev/ttyUSB2', ARDUINO_F, timeout=ARDUINO_TIMEOUT)
                    arduino_run = True
                    logger.info('arduino run!')
                except:
                    arduino_run = False
                    logger.info('arduino not run!')
                    logger.error('no arduino')

else:
    logger.info('no Jetson!')
    try:
        arduino = serial.Serial(ARDUINO_DIR, ARDUINO_F, timeout=ARDUINO_TIMEOUT)
        logger.info('arduino run!')
    except:
        try:
            arduino = serial.Serial('/dev/ttyUSB0', ARDUINO_F, timeout=ARDUINO_TIMEOUT)
            logger.info('arduino run!')
        except:
            try:
                arduino = serial.Serial('/dev/ttyUSB1', ARDUINO_F, timeout=ARDUINO_TIMEOUT)
                logger.info('arduino run!')
            except:
                try:
                    arduino = serial.Serial('/dev/ttyUSB2', ARDUINO_F, timeout=ARDUINO_TIMEOUT)
                    logger.info('arduino run!')
                except:
                    logger.info('arduino not run!')


time.sleep(T30)

def gyro_calib(time_interval_gyro):

    ##calib_gyro = {"ax":0,"ay":0, "az":0,"gx":0,"gy":0, "gz":0}
    time.sleep(time_interval_gyro)
    #BINS_shm = shared_memory.SharedMemory(name='pipeline_bins')
    #C_BINS = np.ndarray((15, N_BINS), dtype=np.float32, buffer=BINS_shm.buf)
    if C_BINS[-1,1]:
        return True
    else:
        logger.error('no gyro calibrate')
        #return False
        return True # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#

def camera_calib(time_calib_cam, vector):
    time.sleep(time_calib_cam)
    dict_calib_cam = {"britnes":0,"smooth":0}

    #CAM_shm = shared_memory.SharedMemory(name='pipeline_cam')
        ## time, frame Quality, Detector,Angle,Distance,problem, calibr, rezerv]
    #C_CAM = np.ndarray((8,N_CAM), dtype=np.float32, buffer=CAM_shm.buf)
    if C_CAM[-1, -2]:
        return True,vector
    else:
        logger.error('no cam calibrate')
        #return False, vector
        return True, vector# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
def frame_distance( dict_calib_cam):
    # = shared_memory.SharedMemory(name='pipeline_cam')
    ## time, frame Quality, Detector,Angle,Distance,problem, calibr, rezerv]
    
    # ??????????????????????????????????????????????????????????????
    #CAM = np.array([[time.time(), 0.,0., 0.,  0., 1., 0., 0.]] * N_CAM)
    #C_CAM = np.ndarray(CAM.shape, dtype=CAM.dtype, buffer=CAM_shm.buf)
    # ??????????????????????????????????????????????????????????????????
    #print("C_CAM:", C_CAM)
    anglex,  r, flag = C_CAM[-1,-5],C_CAM[-1,-4],C_CAM[-1,-6]  #0,0,1 #
    try:
        time_cam, frame_quuality, detect, angle, distance, problem,t,y = C_CAM[-1, :]
        # net_target.get_frame_info()
        ind1 = np.where(C_CAM[:,-6:-3]!=0.0)[0]
        ind0 = np.where(C_CAM[:,-6:-3]==0.0)[0]
        
        if (len(ind0)==CAM.shape[0]):
        	angle, distance = angle, distance
        else:
        	if (len(ind1)>=2):
        		angle, distance = np.mean(C_CAM[ind1,-5]),np.mean(C_CAM[ind1,-4])
        	else:
        		angle, distance =  0,0
        if abs(angle)<10:
        	angle = 0
        if angle<0:
        	angle = angle-10
        logger.info('C_CAM: '+', '.join([str(C_CAM[-1,ib]) for ib in range(C_CAM.shape[1])]))
        logger.info('C_CAM  mean: '+', '.join([str(angle) ,str(distance)]))
        logger.info('C_CAM: ' + str(C_CAM))
        return angle, distance, [detect, frame_quuality]#!!!!!!!!!!!!!! detect
    except:
        return 0, 0, [True, 0]# !!!!!!!!!!!!! False


#arduino = serial.Serial('/dev/ttyACM0', 57600, timeout=1)
def write_point(dir):
    """
    make directory to save info from path
    :param dir:
    :return:
    """
    try:
        shutil.rmtree(dir)
    except:
        pass
    try:
        os.mkdir(dir)
    except:
        pass

    with open(DIR_CAM,'w') as f:
        f.write(dir)
def read_point(dir):
    #read next point
    #  temporary
    nearest_point = [0.0,0.0,0.0]
    return nearest_point
#
def get_control(vector, t01 = T01):
    """

    :param vector:   vector to control
    :param t01:  timeout for Arduino
    :return:
    """
    if ARDUINO_work :
        print(arduino.port)
        print(arduino.is_open)
    for i in range(10):
        #################################### redaction
        if ARDUINO_work :
            try:
                dd = arduino.readline()
                print(str(dd))
            except:
                print('ARDUINO ERROR')
                dd = ''
                logger.error('control from arduino: ERROR')		
        else:
            dd='$1 1 100'
        time.sleep(t01)
        if len(dd)>3:
            break
    #vector = [0, 0, 0, 0, 0, ]
    # v[0] = 1 # moving for target
    # v[1] = 1 # write tragectory
    # v[2] = 1 # write actuator
    # v[3] = 1 # Press B
    # v[4] = 1 # Press A
    # v[5] = 1 # autonom moving
    # v[6] = 1 # calibrate
    # v[7] = 1 # ligth
    # v[8] - usd
    # v[-2] = 1 # error
    # v[-1] = 1 # STOP
    usd = []
    if len(dd)>3:
        vector[-2] = 0
        if ARDUINO_work :
            control_arduino = dd[1:-3].decode().split()
            logger.info('control from arduino: '+str(dd[1:-3].decode()))
        else:
            control_arduino = dd[1:-3].split()
            logger.info('control from arduino: '+str(dd[1:-3]))
        try:
            logger.info('control from arduino: '+str(control_arduino[0])+' '+str(control_arduino[1]))
            logger.info('control from arduino: '+str(dd[1:-3].decode()))
        except:
            pass
        if  control_arduino[0]=='1':
            if control_arduino[1]=='1':
                vector[0] = 1 #moving begin for target
                logger.info('command from arduino 1 1 to start moving to target')
                
            elif control_arduino[1]=='0':
                vector[0] = 0  # stop moving for target
                vector[-1] = 0  # stop moving for target
                logger.info('command from arduino 1 0 to stop moving to target')
            else:
                vector = [0]*len(vector) # stop moving for target
                vector[-2] = 1 #stop target
                logger.info('stop moving for target')
                # for target

        elif control_arduino[0]=='2':
            # for write traectory
            if control_arduino[1]=='1':# begin write tragectory
                vector[1] = 1
                logger.info('command from arduino 2 1 to start writing traectory')
            elif control_arduino[1]=='99':# actuator save
                vector[2] = 1
                logger.info('command from arduino 2 99 to open actuator')
            elif control_arduino[1] == '88':# press B
                vector[3] = 1
                vector[4] = 0
                logger.info('command from arduino 2 88 to write point B')
            elif control_arduino[1] == '77':  # press A
                vector[4] = 1
                vector[3] = 0
                logger.info('command from arduino 2 77 to write point A')
            elif control_arduino[1] == '3':# end write tragectory
                vector[1] = 3
                logger.info('command from arduino 2 3 to stop writing traectory')
            else:
                vector[-2] = 1

        elif control_arduino[0]=='3':
            # for moving about traectory
            if control_arduino[1] == '1' and control_arduino[2] == '0': ## go to A
                vector[5] = 1
                vector[4] = 1
                vector[3] = 0
                logger.info('command from arduino 3 1 0 to start moving to point A')
            elif  control_arduino[1] == '1' and control_arduino[2] == '1' : #go to B
                vector[5] = 1
                vector[4] = 0
                vector[3] = 1
                logger.info('command from arduino 3 1 1 to start moving to point B')
            elif  control_arduino[1] == '2' : #go for current tragectory
                vector[5] = 1
                logger.info('command from arduino 3 2 to return moving to last point')
            elif control_arduino[1] == '0':  # STOP
                logger.info('command from arduino 3 0 to stop moving by traectory')
                vector = [0]*len(vector)
                vector[-1] = 1
            else:
                vector[-2] = 1#error
        elif control_arduino[0] == '4': #calibr
            if control_arduino[1] == '2':
                vector = [0]*len(vector)
                logger.info('message from arduino 4 2 to start writing calib')
                if vector[6] == 0:
                    vector[6] = 1
            elif  control_arduino[1] == '0':
                vector[6] = 0
                vector[-1] = 1
                logger.info('message from arduino 4 0 to stop writing calib')
            else:
                vector[-2] = 1#error

        elif control_arduino[0] == '5':
            # light
            if control_arduino[1] == '2':
                vector[7] = 1
                logger.info('message from arduino 5 2 about turn on light')
            else:
                vector[-2] = 1#error
        elif control_arduino[0] == '6':
            usd = [int(s) for s in control_arduino[1:]]
            logger.info('info from arduino about UZD')
            #USD
        elif control_arduino[0] == '7':
            #stop
            vector = [0]*len(vector)
            vector[-1]=1
            logger.info('message from arduino 7 0 after stop')
        else : # error
            vector[-2] = 1
            
        if (vector[-2] != 1):
            if ARDUINO_work:
                arduino.reset_input_buffer( )
            text = '$100 100 100;'
            logger.info('control to arduino $100 100 100;')
            
            if ARDUINO_work:
                arduino.write(text.encode('utf-8'))
            print(dd)
            print(control_arduino)
            print(text)
            print(vector)
        else:
            logger.info('did not understand command')
            
    else:
        vector = vector
        print(vector)
    return vector, usd

def prepared_mesage(vector, detector_frame, anglex_c,  v_c , t5 = T5):
    """
    #detector_frame, anglex_c,  r_c = [1,0], 0., 0.,
    Args:
        detector_frame: detector coord detector_frame[0] - target in frame, detector_frame[1] - 1 light on, 0 - light off
        anglex_c, - angle or control teg
        v_c - velosity or actuator
        t5 - timeout to write point
        T4 - timeout to rotate
        T01 - timeout to Arduino

    #vector = [0, 0, 0, 0, 0, ]
    # v[0] = 1 # moving for target 0 - stop, 1 - move
    # v[1] = 1 # write tragectory, 0 - stop, 1- start write, 2 - write in process , 3- stop write
    # v[2] = 1 # write actuator
    # v[3] = 1 # Press B
    # v[4] = 1 # Press A
    # v[5] = 1 # autonom moving
    # v[6] = 1 # calibrate : 0 no calibrate , 1- start calibrate, 2 have calibrate
    # v[7] = 1 # ligth
    # v[8] - usd
    # v[-2] = 1 # error
    # v[-1] = 1 # STOP

    Returns:

    """
    #################################### redaction

    def send_message(control_message,s1,s2,s3='', t = T01):
        control_message[1] = s1
        control_message[3] = s2  # 0 - stop, 1- midle speed, 2 - fast speed
        control_message[5] = s3  # -90 : 90 ^ 10 angle
        text = ''.join(control_message)
        if ARDUINO_work :
            f = arduino.write(text.encode('utf-8'))
        time.sleep(t)
        logger.info('control to arduino: '+''.join(control_message))
        print(control_message)


    control_message = ['$', '', ' ', '', ' ', '', ';']


    #moving by target
    if vector[0]==1 and detector_frame[0]:
        # send_message(control_message, '1', str(int(v_c)), s3=str(int(anglex_c)))
        #'1', str(v_c) # 0 - stop, 1- midle speed, 2 - fast speed # -90 : 90 ^ 10 angle
        
        if (int(anglex_c) != 0):# and (T_ANGLE == 0):
            send_message(control_message, '1', '1', s3=str(int(anglex_c)))
            send_message(control_message, '1', str(int(v_c)), s3=str(int(0)))
            logger.info('mowing to target angle = 0: ' + str(int(v_c)) + ' ' + str(int(anglex_c)))
            time.sleep(T4*abs(int(anglex_c)//10 + 2))
            T_ANGLE = 1
        else:
            T_ANGLE = 0
            send_message(control_message, '1', str(int(v_c)), s3=str(int(anglex_c)))
            logger.info('mowing to target angle = 0: ' + str(int(v_c)) + ' ' + str(int(anglex_c)))
            time.sleep(T4)
        
        logger.info('mowing to target: ' + str(int(v_c)) + ' ' + str(int(anglex_c)))

    elif  vector[0]==1:
        #emergence stop
        send_message(control_message, '1', '11', s3='0')# hand work
        vector[0] = 0 # STOP
        
        logger.info('want manual')

    ## write tragectory
    if vector[1]==1:
        # agree write
        ##### write A
        send_message(control_message, '2', '2', s3='0')
        write_point(DIR_A)
        time.sleep(t5)

        write_point(DIR_AB) # directori name to camera
        send_message(control_message, '2', '7', s3='0')# turn on light A
        vector[1] = 2
        logger.info('start writing traectory, it is A')

    if vector[1]==2 and vector[3]==1:
        ##### write B
        write_point(DIR_B)
        time.sleep(t5)
        send_message(control_message, '2', '8', s3='0')# turn on light B
        write_point(DIR_BA)# directori name to camera from point B to point A
        vector[3] = 2
        logger.info('writing traectory, it is B')

    if vector[1]==2 and vector[4]==1:
        ##### write A'
        send_message(control_message, '2', '7', s3='0')  ## turn on light B
        write_point(DIR_A1)
        time.sleep(t5)
        write_point('')

        vector[4] = 2
        logger.info('writing traectory, it is A')
        

    if vector[1]==3 :
        ##### end write tragectory

        # turn on light M - end write
        send_message(control_message, '2', '9', s3='0')
        write_point('')
        vector[1] = 0
        logger.info('stop writing traectory')
        

    if vector[5]==1 and vector[4] == 1: # moving to A
        ##### temetry to arduino
        #send_message(control_message, '3', str(int(v_c)), s3=str(int(anglex_c)))

        if int(anglex_c) != 0:
            send_message(control_message, '3', '1', s3=str(int(anglex_c)))
            send_message(control_message, '3', str(int(v_c)), s3=str(int(0)))
            time.sleep(T4*abs(int(anglex_c)//10))
        else:
            send_message(control_message, '3', str(int(v_c)), s3=str(int(anglex_c)))

        #'3'#str(v_c) # 0 - stop, 1- midle speed, 2 - fast speed ,  99 - actuator onn, 11 - manual control, 9 - find traectory, 8 - point A
        # #str(anglex_c)  # -90 : 90 ^ 10 angle
        logger.info('B-A traectory moving: '+ str(int(v_c)) + ' ' + str(int(anglex_c)))

    if vector[5]==1 and vector[3] == 1: # moving to B
        ##### temetry to arduino
        #send_message(control_message, '3', str(int(v_c)), s3=str(int(anglex_c)))
        
        if int(anglex_c) != 0:
            send_message(control_message, '3', '1', s3=str(int(anglex_c)))
            send_message(control_message, '3', str(int(v_c)), s3=str(int(0)))
            time.sleep(T4*abs(int(anglex_c)//10))
        else:
            send_message(control_message, '3', str(int(v_c)), s3=str(int(anglex_c)))

        #'3'#str(v_c) # 0 - stop, 1- midle speed, 2 - fast speed ,  99 - actuator onn, 11 - manual control, 9 - find traectory, 8 - point A
        # #str(anglex_c)  # -90 : 90 ^ 10 angle
        
        logger.info('A-B traectory moving: '+ str(int(v_c)) + ' ' + str(int(anglex_c)))

    if vector[6]==0 :
        ##### no calibrate - I want calibrate
        #send_message(control_message, '4', '4', s3='0') #
        vector[6]=1#!##
        logger.info('I want to CALIB')

    if vector[6]==1 :
        ##### calibrate
        write_point(DIR_CALIB)
        calib_range = ['-10']*9 + ['10']*18 + ['-10']*9

        for i in calib_range:
            #send_message(control_message, '4', '0', s3=str(i), t=T5)
            
            logger.info('calib in process, '+str(i))
            #'4'#'0'  #i =  -90 : 90 ^ 10 angle
            #********************** redaction
        vector[6] = 2
        #send_message(control_message, '4', '3', s3='0')# calibrate end
        
        logger.info('jetson made calib')
        write_point('') # stop write calib

    if vector[7]==0 and detector_frame[1]==0 :  #08.06.23 ==0 for detector
        ##### light test
        send_message(control_message, '5', '1', s3='0')# turn on light
        
        logger.info('jetson cam need light')

    if vector[7]==1 and detector_frame[1]==0 :  #08.06.23 ==0 for detector
        #emergence stop # too dark
        send_message(control_message, '1', '11', s3='0')
        vector[0] = 0 # STOP
        
        logger.info('want manual')

    if vector[8]==1 :
        ##### USD query
        send_message(control_message, '6', '1', s3='0')
        logger.info('ask arduino about UZD')

    if vector[-1]==1 :
        ##### STOP
        send_message(control_message, '7', '0', s3='0')
        vector[-1] = 0
        logger.info('PREPIATSTVIE near us')

    return vector, control_message

# main controll sender
# gyro take
def gyro_distance():
    ax, ay, az, wx, wy, wz, x, y, z = 0,0,0,0,0,0,0,0,0
    #BINS_shm = shared_memory.SharedMemory(name='pipeline_bins')
    #C_BINS = np.ndarray((15, N_BINS), dtype=float32np., buffer=BINS_shm.buf)
    #print("C_BINS:", C_BINS)

    try:
        time_bins, x, y, z, vx, vy, vz, ax, ay, az, lx, ly, lz = C_BINS[-1, :13]
        
        if (lx > 12/180*np.pi):
            logger.info('big angle x: '+str(lx))
        
        if (ly > 12/180*np.pi):
            logger.info('big angle y: '+str(ly))
        
        if (lz > 12/180*np.pi):
            logger.info('big angle z: '+str(lz))

    except:
        time_bins, x, y, z, vx, vy, vz, ax, ay, az, lx, ly, lz = time.time(), 0,0,0,0,0,0,0,0,0,0,0,0
    return ax,ay,az,wx, wy, wz,x,y,z

#  main pipeline
def to_control(vector, detector_frame,  xwa, r_c = 0):
    """
    contron motomul
    Args:
        detector_frame: detector coord detector_frame[0] - target in frame, detector_frame[1] - 1 light on, 0 - light off
        frame: image
        xwa: gyro

    #vector = [0, 0, 0, 0, 0, ]
    # v[0] = 1 # moving for target 0 - stop, 1 - move
    # v[1] = 1 # write tragectory, 0 - stop, 1- start write, 2 - write in process , 3- stop write
    # v[2] = 1 # write actuator
    # v[3] = 1 # Press B
    # v[4] = 1 # Press A
    # v[5] = 1 # autonom moving
    # v[6] = 1 # calibrate : 0 no calibrate , 1- start calibrate, 2 have calibrate
    # v[7] = 1 # ligth
    # v[8] - usd
    # v[-2] = 1 # error
    # v[-1] = 1 # STOP

    Returns:

    """
    #################################### redaction
    logger.info('to_control1^ vector:'+', '.join([str(x) for x in vector]) + "_")
    if (vector[0]==1) or (vector[1]==0) or (vector[5]==1) or (vector[6]==1):#(vector[0]==1) or (vector[1]==1) or (vector[5]==1) or (vector[6]==1)
        logger.info('to_control2:')
        # create control
        detect, anglex_c, r_c = control.kate_system_control(detector_frame,  xwa, v=r_c, logger =  logger)
        logger.info('to_control3 vector:'+str(detect)+str(anglex_c)+str(r_c))
        if not detect[0]:
            # to do: go to traectory
            # STOP
            detect, anglex_c, r_c = [1, 0], 0., 0.,
    elif (vector[1] == 1) or (vector[2] == 1) :
        # create control autonomus
        logger.info('to_control4 vector:'+str(detector_frame[0])+str(detector_frame[1])+str(detector_frame[2]))
        detect, anglex_c, r_c = control.kate_system_control_auto(detector_frame, xwa, v=r_c, arduino = arduino, logger =  logger)
        logger.info('to_control5 vector:'+str(detect)+str(anglex_c)+str(r_c))
    else:
        logger.info('to_control6 vector:')
        detect, anglex_c, r_c = 0, 0., 0.,
        logger.info('to_control7 vector:'+str(detect)+str(anglex_c)+str(r_c))
    return detect, anglex_c,  r_c

# Check if the webcam is opened correctly

# calibrate image
vector = [0]*9

#  MAIN PROCESS
if config.JETSON != 2:

    calib_gyro = gyro_calib(time_calib_gyro )
    calib, vector = 1,np.array([1,0,0,0,0,0,1.,1])
    FQ = 1 #camera_calib(time_calib_cam, vector)

    print('calibr end')

    time.sleep(T3)

    # track and
    control_vector = [0]*11
    control_vector[6]=0
    control_vector[1]=0


    T0 =  time.time

    #with keyboard.Events() as events:
    r_c = 0 # arduino velocity (0,1,2)
    ks = 0
    #  MAIN CYCLE
    while True:

            # frame

            # detect
            control_vector,usd = get_control(control_vector)
            logger.info('control vector:'+', '.join([str(x) for x in control_vector])+"_")
            # target distance
            ############ !!!!!!!!!!!!!!!!!!!!!!!!!!!! clear with natural test
            #control_vector[0] = 1
            #try:
            if 1:
                anglex, r, flag = frame_distance( calib)
                logger.info('jetson cam:'+str(anglex)+"_"+str(r)+"_"+str(flag))
                detector_frame = (anglex, r, flag)

                x,y,z, ax,ay,az,wx, wy, wz = gyro_distance()
                xwa = (x,y,z, ax,ay,az,wx, wy, wz)
                # create control
                print('++++++++++++++++++++++++++++++++++++++++++++++')
                print('main paipl:'+', '.join([str(x) for x in xwa]) + ', '.join([str(x) for x in detector_frame]))
                logger.info('bins: '+', '.join([str(x) for x in xwa]))
                detect, anglex_c, r_c = to_control(control_vector, detector_frame,  xwa, r_c = r_c)

                logger.info('cam: '+'main paipl:'+', '.join([str(x) for x in (anglex_c,r_c)]))
                #FQ = 1
                control_vector, control_message = prepared_mesage(control_vector, [detect, FQ], anglex_c,  r_c)
                logger.info('jetson control vector:' + '_'.join([str(v) for v in control_vector]))                #message = arduino_test.prepared_mesage(detect, anglex_c, r_c)
                #success = arduino_test.controller_mesage(message)
                if control_message[3] == '1':
                    ks += 1
                    logger.info('KS: '+str(ks) )

                #if ks == 50:
                    #control_vector[0] =  1
                    #_,_ = prepared_mesage(control_vector, [0, FQ], 0, 0)
                    #logger.info('STOP: '+str(ks) )
                    #arduino.close()
                    #break
            #except:
            else:
                print('error')


    if ARDUINO_work :
        arduino.close()
#BINS_shm.close()
#CAM_shm.close()
#CAM_shm.unlink()
#BINS_shm.unlink()
